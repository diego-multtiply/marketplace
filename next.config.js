/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "multtiplyarquivos.s3.sa-east-1.amazonaws.com",
      "mkt.multtiply.com",
      "multtiply.xpendi.com.br",
      "cyhnwguzha.cloudimg.io",
    ],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  typescript: {
    ignoreBuildErrors: true,
  },
};

module.exports = nextConfig;
