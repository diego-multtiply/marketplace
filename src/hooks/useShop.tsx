import { IProduct } from "@/common/types/products";
import { useRouter } from "next/router";
import { useAppDispatch, useAppSelector } from "./useStore";
import { addProductToCart, getProvidersCart } from "@/server/services/shop";

import { toast } from "react-toastify";

import CartPopup from "@/components/features/product/common/cart-popup";
import { setProviders, setTotalProducts } from "@/store/cart";
import { formatObject } from "@/utils";

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  const { user, isLogged } = useAppSelector((state) => state.auth);
  const router = useRouter();

  const dispatch = useAppDispatch();

  const formatQuery = (field: string, value: string) => {
    const nQuery = router.query;

    nQuery[field] = value;

    return nQuery;
  };

  const getProductPrice = (item: IProduct) => {
    let price;

    if (user?.pessoa?.enderecos.length) {
      const mainAddress = user?.pessoa?.enderecos.find(
        (item) => item.principal === "S"
      );

      if (mainAddress) {
        // Busca por franquia e estado
        price = item?.tabelaPreco?.find(
          (tabelaPreco) =>
            tabelaPreco.estado === mainAddress.estado.sigla &&
            tabelaPreco.idfranquia === user.pessoa.cliente?.idfranquia
        );

        if (!price) {
          // Busca apenas franquia
          price = item?.tabelaPreco?.find(
            (tabelaPreco) =>
              tabelaPreco.estado === null &&
              tabelaPreco.idfranquia === user.pessoa.cliente?.idfranquia
          );
        }

        if (!price) {
          // Busca por franquia geral e estado
          price = item?.tabelaPreco?.find(
            (tabelaPreco) =>
              tabelaPreco.estado === mainAddress.estado.sigla &&
              tabelaPreco?.franquia_geral === "S"
          );
        }

        if (!price) {
          // Busca por franquia geral e estado
          price = item?.tabelaPreco?.find(
            (tabelaPreco) =>
              tabelaPreco.estado === null && tabelaPreco?.franquia_geral === "S"
          );
        }

        return price?.preco;
      }
    } else {
      // Busca apenas por franquia
      price = item.tabelaPreco.find(
        (tabelaPreco) =>
          tabelaPreco.estado === null &&
          tabelaPreco.idfranquia === user?.pessoa.cliente?.idfranquia
      );

      if (!price) {
        // Busca apenas por franquia geral
        price = item.tabelaPreco.find(
          (tabelaPreco) =>
            tabelaPreco.estado === null && tabelaPreco?.franquia_geral === "S"
        );
      }

      return price?.preco;
    }

    return 0;
  };

  async function addToCart(p: IProduct, quantidade: number) {
    const { data } = await addProductToCart(
      p.idfornecedor,
      p.id,
      p?.idprodutoBase || null,
      quantidade
    );

    if (data.success) {
      if (isLogged) {
        const { data: providersCartData } = await getProvidersCart();

        if (user?.pessoa.fornecedor) {
          dispatch(
            setTotalProducts(
              formatObject(providersCartData.data[0])?.quantidade_produtos
            )
          );
        } else {
          dispatch(setProviders(providersCartData.data));
        }
      }

      toast(<CartPopup product={{ ...p, qty: quantidade }} />, {
        position: "top-right",
      });
    }
  }

  return { formatQuery, getProductPrice, addToCart };
};
