export interface IClient {
  id: number;
  idfranquia: number;
  idpessoa: number;
  limite_compra: number;
  limite_credito: number;
}

export interface IState {
  id: number;
  nome: string;
  idpais: number;
  sigla: string;
  codigo_ibge: number;
}

export interface IAddress {
  ativo: string;
  bairro: string;
  cep: string;
  complemento: string;
  estado: IState;
  id: number;
  idcidade: number;
  idestado: number;
  idpais: number;
  idpessoa: number;
  numero: string;
  principal: string;
  rua: string;
}

export interface IPerson {
  aprovado: string;
  ativo: string;
  bloqueado: string;
  bloqueado_parcial: string;
  cnpj: string;
  cpf: string;
  enderecos: IAddress[];
  fantasia: string;
  id: number;
  ie: number;
  observacoes: string;
  possui_representante_financeiro: string;
  razao_social: string;
  regime_tributario: string;
  representante_financeiro: number;
  responsavel: string;
  rg: string;
  tipo: string;
  cliente?: IClient;
  fornecedor?: IClient;
  representante?: IClient;
}

export interface IUser {
  nome: string;
  idfranquia?: number;
  estado?: string;
  pessoa: IPerson;
}
