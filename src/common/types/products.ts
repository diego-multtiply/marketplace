export interface IProductVariations {
  cor: string;
  ean: number;
  id: number;
  idproduto_base: number;
  nome: string;
  numeracao: string;
  slug: string;
  tamanho: string;
}

export interface IPorductPrice {
  id: number;
  idproduto: number;
  idfranquia: number;
  franquia: string;
  idestado: number;
  estado: string;
  preco: number;
  preco_venda: number;
  franquia_geral: string;
}

export interface IProductImage {
  id: number;
  idproduto: number;
  url: string;
  principal: string;
}

export interface IProduct {
  id: number;
  nome: string;
  slug: string;
  sku: number;
  ean: number;
  unidade: string;
  marca: string;
  peso_liquido: number;
  peso_bruto: number;
  volumes: number;
  altura: number;
  largura: number;
  profundidade: number;
  descricao: string;
  descricao_complementar: string;
  observacoes: string;
  idcategoria_departamento: number;
  descricao_departamento: string;
  slug_departamento: string;
  idcategoria_grupo_principal: number;
  descricao_grupo_principal: string;
  slug_grupo_principal: string;
  idcategoria_grupo_secundario: number;
  descricao_grupo_secundario: string;
  slug_grupo_secundario: string;
  link_externo: string;
  idfornecedor: number;
  razao_social_fornecedor: string;
  importado: string;
  cubagem: number;
  ncm: string;
  idcolecao: number;
  descricao_colecao: string;
  cores: string;
  tamanhos: string;
  numeracao: string;
  imagens: IProductImage[];
  tabelaPreco: IPorductPrice[];
  variacoes: IProductVariations[];
  idprodutoBase?: number | null;
  preco?: number;
  preco_minimo?: number;
  preco_maximo?: number;
}
