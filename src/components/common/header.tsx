import { useEffect } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import ALink from "@/components/features/custom-link";

import CartMenu from "@/components/common/partials/cart-menu";
import MainMenu from "@/components/common/partials/main-menu";
import SearchBox from "@/components/common/partials/search-box";
import LoginModal from "@/components/features/modals/login-modal";

import { headerBorderRemoveList } from "@/utils/data/menu";
import { useAppSelector } from "@/hooks/useStore";
import { IDepartamento, IGrupoPrincipal } from "@/store/shop";

export default function Header() {
  const router = useRouter();

  const { departamentos } = useAppSelector((state) => state.shop);
  const { isLogged } = useAppSelector((state) => state.auth);

  useEffect(() => {
    let header = document.querySelector("header");
    if (header) {
      if (
        headerBorderRemoveList.includes(router.pathname) &&
        header.classList.contains("header-border")
      )
        header.classList.remove("header-border");
      else if (!headerBorderRemoveList.includes(router.pathname))
        document.querySelector("header")?.classList.add("header-border");
    }
  }, [router.pathname]);

  const showMobileMenu = () => {
    document.querySelector("body")?.classList.add("mmenu-active");
  };

  const renderGrupoSecundario = (grupoPrincipal: IGrupoPrincipal) => {
    return grupoPrincipal.grupoSecundario.map((item, index) => (
      <li key={String(index)}>
        <ALink
          href={{
            pathname: "/shop",
            query: { category: item.id },
          }}
        >
          {item.descricao}
        </ALink>
      </li>
    ));
  };

  const renderGrupoPrincipal = (departamento: IDepartamento) => {
    return departamento.grupoPrincipal
      .filter((item) => item.grupoSecundario.length > 0)
      .map((item, index) => (
        <li key={String(index)}>
          <h4 className="menu-title">{item.descricao}</h4>
          <hr className="divider" />
          <ul>{renderGrupoSecundario(item)}</ul>
        </li>
      ));
  };

  const renderDepartamentos = () => {
    return departamentos?.map((item, index) => (
      <li className="submenu" key={String(index)}>
        <ALink
          className={
            router.query.category === String(item.slug) ? "active" : ""
          }
          href={{
            pathname: "/shop",
            query: { category: item.slug },
          }}
          scroll={false}
        >
          {item.descricao}
        </ALink>
        <ul className="megamenu">
          {renderGrupoPrincipal(item)}
          <li>
            {/* <div className="banner-fixed menu-banner menu-banner4 content-middle">
              <figure>
                <Image
                  src="/images/home/menu/banner-4.jpg"
                  alt="Menu Banner"
                  width="235"
                  height="347"
                />
              </figure>
              <div className="banner-content">
                <h4 className="banner-subtitle mb-2 font-weight-normal">
                  Trending Now
                </h4>
                <h3 className="banner-title ls-m">
                  New
                  <br />
                  Summer
                  <br />
                  Fashion
                </h3>
                <ALink
                  href="/shop"
                  className="btn btn-outline btn-dark btn-rounded btn-md"
                >
                  Shop Now
                </ALink>
              </div>
            </div> */}
          </li>
        </ul>
      </li>
    ));
  };

  return (
    <header className="header">
      <div className="header-top">
        <div className="container">
          <div className="header-left">
            <p className="welcome-msg ls-normal">Bem Vindo a Multiweb!</p>
          </div>
          <div className="header-right">
            {/* <div className="dropdown">
              <ALink href="#">USD</ALink>
              <ul className="dropdown-box">
                <li>
                  <ALink href="#">USD</ALink>
                </li>
                <li>
                  <ALink href="#">EUR</ALink>
                </li>
              </ul>
            </div>

            <div className="dropdown ml-5">
              <ALink href="#">ENG</ALink>
              <ul className="dropdown-box">
                <li>
                  <ALink href="#">ENG</ALink>
                </li>
                <li>
                  <ALink href="#">FRH</ALink>
                </li>
              </ul>
            </div> */}

            {/* <span className="divider"></span> */}
            {/* <ALink href="#" className="contact d-lg-show">
              <i className="d-icon-map"></i>Vendors
            </ALink>
            <ALink href="/pages/account" className="help d-lg-show">
              <i className="d-icon-info"></i> My Account
            </ALink> */}
            <LoginModal />
          </div>
        </div>
      </div>

      <div className="header-middle">
        <div className="container">
          <div className="header-left mr-4">
            <ALink
              href="#"
              className="mobile-menu-toggle"
              onClick={showMobileMenu}
            >
              <i className="d-icon-bars2"></i>
            </ALink>

            <ALink href="/" className="logo">
              <Image
                src="/images/logo-principal.png"
                alt="logo"
                width="153"
                height="44"
              />
            </ALink>

            <SearchBox />
          </div>

          <div className="header-right">
            <div className="icon-box icon-box-side">
              <div className="icon-box-icon mr-0 mr-lg-2">
                <i className="d-icon-phone"></i>
              </div>
              <div className="icon-box-content d-lg-show">
                {/* <h4 className="icon-box-title text-dark text-normal">
                  <ALink
                    href="mailto:riode@mail.com"
                    className="text-primary d-inline-block"
                  >
                    Live chat
                  </ALink>{" "}
                  or:
                </h4> */}
                <p>
                  <span>(55) 99988-1985</span>
                </p>
              </div>
            </div>
            {/* <span className="divider mr-4"></span> */}
            {/* <ALink href="#" className="compare">
              <i className="d-icon-compare"></i>
            </ALink> */}
            {/* <ALink href="/pages/wishlist" className="wishlist">
              <i className="d-icon-heart"></i>
            </ALink> */}
            {isLogged && (
              <>
                <span className="divider"></span>
                <CartMenu />
              </>
            )}
          </div>
        </div>
      </div>

      <div className="header-bottom sticky-header fix-top sticky-content d-lg-show">
        <div className="container">
          <div className="header-left">
            <div className="dropdown category-dropdown" data-visible="true">
              <ALink href="#" className="category-toggle" title="Departamentos">
                <i className="d-icon-bars"></i>
                <span>Departamentos</span>
              </ALink>

              <div className="dropdown-box">
                <ul className="menu vertical-menu category-menu">
                  {renderDepartamentos()}
                  {/* <li>
                    <ALink
                      href="/shop"
                      className="font-weight-semi-bold text-primary text-uppercase ls-25"
                    >
                      Ver todos Departamentos
                      <i className="d-icon-angle-right"></i>
                    </ALink>
                  </li> */}
                </ul>
              </div>
            </div>

            <MainMenu />
          </div>
          {/* <div className="header-right">
            <ALink href="#">
              <i className="d-icon-card"></i>Ofertas Especiais
            </ALink>
          </div> */}
        </div>
      </div>
    </header>
  );
}
