import React from "react";

const PageLoading: React.FC = () => {
  return (
    <div className="page-loading">
      <div className="loading-overlay">
        <div className="bounce-loader">
          <div className="bounce1"></div>
          <div className="bounce2"></div>
          <div className="bounce3"></div>
          <div className="bounce4"></div>
        </div>
      </div>
    </div>
  );
};

export default PageLoading;
