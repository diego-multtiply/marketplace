// @ts-nocheck
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Tabs, Tab, TabList, TabPanel } from "react-tabs";

import ALink from "@/components/features/custom-link";
import Card from "@/components/features/accordion/card";

import { mainMenu } from "@/utils/data/menu";
import { useAppSelector } from "@/hooks/useStore";
import { IDepartamento, IGrupoPrincipal } from "@/store/shop";

function MobileMenu(props) {
  const [search, setSearch] = useState("");
  const [timer, setTimer] = useState(null);
  const router = useRouter();

  const { departamentos } = useAppSelector((state) => state.shop);

  useEffect(() => {
    window.addEventListener("resize", hideMobileMenuHandler);
    document.querySelector("body")?.addEventListener("click", onBodyClick);

    return () => {
      window.removeEventListener("resize", hideMobileMenuHandler);
      document.querySelector("body")?.removeEventListener("click", onBodyClick);
    };
  }, []);

  useEffect(() => {
    setSearch("");
  }, [router.query.slug]);

  const hideMobileMenuHandler = () => {
    if (window.innerWidth > 991) {
      document.querySelector("body")?.classList.remove("mmenu-active");
    }
  };

  const hideMobileMenu = () => {
    document.querySelector("body")?.classList.remove("mmenu-active");
  };

  function onSearchChange(e) {
    setSearch(e.target.value);
  }

  function onBodyClick(e) {
    if (e.target.closest(".header-search"))
      return (
        e.target.closest(".header-search").classList.contains("show-results") ||
        e.target.closest(".header-search").classList.add("show-results")
      );

    document.querySelector(".header-search.show") &&
      document.querySelector(".header-search.show")?.classList.remove("show");
    document.querySelector(".header-search.show-results") &&
      document
        .querySelector(".header-search.show-results")
        ?.classList.remove("show-results");
  }

  function onSubmitSearchForm(e) {
    e.preventDefault();
    router.push({
      pathname: "/shop",
      query: {
        search: search,
      },
    });
  }

  const renderGrupoSecundario = (grupoPrincipal: IGrupoPrincipal) => {
    return grupoPrincipal.grupoSecundario.map((item, index) => (
      <li key={String(index)}>
        <ALink
          href={{
            pathname: "/shop",
            query: { category: item.slug },
          }}
        >
          {item.descricao}
        </ALink>
      </li>
    ));
  };

  const renderGrupoPrincipal = (departamento: IDepartamento) => {
    return departamento.grupoPrincipal.map((item, index) => (
      <li key={String(index)}>
        <Card
          title={item.descricao}
          type="mobile"
          url={{
            pathname: "/shop",
            query: { category: item.slug },
          }}
        >
          <ul>{renderGrupoSecundario(item)}</ul>
        </Card>
      </li>
    ));
  };

  const renderDepartamentos = () => {
    return departamentos?.map((item, index) => (
      <li key={String(index)}>
        <Card
          title={item.descricao}
          type="mobile"
          url={{
            pathname: "/shop",
            query: { category: item.slug },
          }}
        >
          <ul>{renderGrupoPrincipal(item)}</ul>
        </Card>
      </li>
    ));
  };

  return (
    <div className="mobile-menu-wrapper">
      <div className="mobile-menu-overlay" onClick={hideMobileMenu}></div>

      <ALink className="mobile-menu-close" href="#" onClick={hideMobileMenu}>
        <i className="d-icon-times"></i>
      </ALink>

      <div className="mobile-menu-container scrollable">
        <form
          action="#"
          className="input-wrapper"
          onSubmit={onSubmitSearchForm}
        >
          <input
            type="text"
            className="form-control"
            name="search"
            autoComplete="off"
            value={search}
            onChange={onSearchChange}
            placeholder="Pesquisar..."
          />
          <button className="btn btn-search" type="submit">
            <i className="d-icon-search"></i>
          </button>
        </form>

        <Tabs
          selectedTabClassName="show"
          selectedTabPanelClassName="active"
          defaultIndex={0}
        >
          <div className="tab tab-nav-simple tab-nav-center tab-nav-boxed">
            <TabList className="nav nav-tabs nav-fill" role="tablist">
              <Tab className="nav-item">
                <span className="nav-link">Menu</span>
              </Tab>
              <Tab className="nav-item">
                <span className="nav-link">Departamentos</span>
              </Tab>
            </TabList>
          </div>

          <div className="mobile-menu mmenu-anim tab-content">
            <TabPanel className="tab-pane">
              <ul>
                <li>
                  <ALink href="/">Início</ALink>
                </li>

                <li>
                  <ALink href="/pages/about-us">Sobre A Multiweb</ALink>
                </li>

                {/* <li>
                  <Card title="Shop" type="mobile" url="/shop">
                    <ul>
                      <li>
                        <Card title="Variations 1" type="mobile">
                          <ul>
                            {mainMenu.shop.variation1.map((item, index) => (
                              <li key={`shop-${item.title}`}>
                                <ALink href={"/" + item.url}>
                                  {item.title}
                                  {item.hot ? (
                                    <span className="tip tip-hot">Hot</span>
                                  ) : (
                                    ""
                                  )}
                                </ALink>
                              </li>
                            ))}
                          </ul>
                        </Card>
                      </li>
                      <li>
                        <Card title="Variations 2" type="mobile">
                          <ul>
                            {mainMenu.shop.variation2.map((item, index) => (
                              <li key={`shop-${item.title}`}>
                                <ALink href={"/" + item.url}>
                                  {item.title}
                                  {item.new ? (
                                    <span className="tip tip-new">New</span>
                                  ) : (
                                    ""
                                  )}
                                </ALink>
                              </li>
                            ))}
                          </ul>
                        </Card>
                      </li>
                    </ul>
                  </Card>
                </li>

                <li>
                  <Card title="Vendor" type="mobile" url="/vendor">
                    <ul>
                      <li>
                        <ALink href="/vendor">Store List</ALink>
                      </li>
                      <li>
                        <ALink href="/vendor/single">Vendor Store</ALink>
                      </li>
                    </ul>
                  </Card>
                </li>

                <li>
                  <Card
                    title="Products"
                    type="mobile"
                    url="/product/default/fashionable-leather-satchel"
                  >
                    <ul>
                      <li>
                        <Card title="Product Pages" type="mobile">
                          <ul>
                            {mainMenu.product.pages.map((item, index) => (
                              <li key={`product-${item.title}`}>
                                <ALink href={"/" + item.url}>
                                  {item.title}
                                  {item.hot ? (
                                    <span className="tip tip-hot">Hot</span>
                                  ) : (
                                    ""
                                  )}
                                </ALink>
                              </li>
                            ))}
                          </ul>
                        </Card>
                      </li>

                      <li>
                        <Card title="Product Layouts" type="mobile">
                          <ul>
                            {mainMenu.product.layout.map((item, index) => (
                              <li key={`product-${item.title}`}>
                                <ALink href={"/" + item.url}>
                                  {item.title}
                                  {item.new ? (
                                    <span className="tip tip-new">New</span>
                                  ) : (
                                    ""
                                  )}
                                </ALink>
                              </li>
                            ))}
                          </ul>
                        </Card>
                      </li>
                    </ul>
                  </Card>
                </li>

                <li>
                  <Card title="Pages" type="mobile" url="/pages/about-us">
                    <ul>
                      {mainMenu.other.map((item, index) => (
                        <li key={`other-${item.title}`}>
                          <ALink href={"/" + item.url}>
                            {item.title}
                            {item.new ? (
                              <span className="tip tip-new">New</span>
                            ) : (
                              ""
                            )}
                          </ALink>
                        </li>
                      ))}
                    </ul>
                  </Card>
                </li>

                <li>
                  <Card title="Blog" type="mobile" url="/blog/classic">
                    <ul>
                      {mainMenu.blog.map((item, index) =>
                        item.subPages ? (
                          <li key={"blog" + item.title}>
                            <Card
                              title={item.title}
                              url={"/" + item.url}
                              type="mobile"
                            >
                              <ul>
                                {item.subPages.map((item, index) => (
                                  <li key={`blog-${item.title}`}>
                                    <ALink href={"/" + item.url}>
                                      {item.title}
                                    </ALink>
                                  </li>
                                ))}
                              </ul>
                            </Card>
                          </li>
                        ) : (
                          <li
                            key={"blog" + item.title}
                            className={item.subPages ? "submenu" : ""}
                          >
                            <ALink href={"/" + item.url}>{item.title}</ALink>
                          </li>
                        )
                      )}
                    </ul>
                  </Card>
                </li>

                <li>
                  <Card title="elements" type="mobile" url="/elements">
                    <ul>
                      {mainMenu.element.map((item, index) => (
                        <li key={`elements-${item.title}`}>
                          <ALink href={"/" + item.url}>{item.title}</ALink>
                        </li>
                      ))}
                    </ul>
                  </Card>
                </li>

                <li>
                  <a
                    href="https://d-themes.com/buynow/riodereact"
                    target="_blank"
                  >
                    Buy Riode!
                  </a>
                </li> */}
              </ul>
            </TabPanel>
            <TabPanel className="tab-pane">
              <ul>{renderDepartamentos()}</ul>
            </TabPanel>
          </div>
        </Tabs>
      </div>
    </div>
  );
}

export default React.memo(MobileMenu);
