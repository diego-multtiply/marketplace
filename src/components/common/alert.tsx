import React from "react";

declare interface Props {
  text: string;
  type: "warning";
  className?: string;
}

const Alert: React.FC<Props> = ({ text, type, className, ...props }) => {
  const renderClass = () => {
    if (type === "warning") {
      return "alert-warning";
    }
  };

  const renderIcon = () => {
    if (type === "warning") {
      return <i className="fas fa-exclamation-circle"></i>;
    }
  };

  return (
    <div
      className={`alert alert-light ${renderClass()} alert-icon ${className}`}
      {...props}
    >
      {renderIcon()}
      <span className="text-body">{text}</span>
    </div>
  );
};

export default Alert;
