import { useEffect, useMemo } from "react";
import { connect } from "react-redux";
import { useRouter } from "next/router";
import Modal from "react-modal";

import { closeModal } from "@/store/modal";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";

const customStyles = {
  content: {
    position: "relative",
  },
  overlay: {
    background: "rgba(0,0,0,.4)",
    overflowX: "hidden",
    display: "flex",
    overflowY: "auto",
    opacity: 0,
  },
};

Modal.setAppElement("#__next");

function VideoModal() {
  const router = useRouter();

  const { openModal, singleSlug } = useAppSelector((state) => state.modal);

  const dispatch = useAppDispatch();

  const isOpen = useMemo(() => {
    return openModal;
  }, [openModal]);

  useEffect(() => {
    dispatch(closeModal());

    // router.events.on("routeChangeStart", closeModal);

    // return () => {
    //   router.events.off("routeChangeStart", closeModal);
    // };
  }, [router.events]);

  useEffect(() => {
    if (isOpen)
      setTimeout(() => {
        document.querySelector(".ReactModal__Overlay").classList.add("opened");
      }, 100);
  }, [isOpen]);

  const closeVideo = () => {
    document.querySelector(".ReactModal__Overlay").classList.add("removed");
    document.querySelector(".ReactModal__Overlay").classList.remove("opened");
    document
      .querySelector(".video-modal.ReactModal__Content")
      .classList.remove("ReactModal__Content--after-open");
    document
      .querySelector(".video-modal-overlay.ReactModal__Overlay")
      .classList.remove("ReactModal__Overlay--after-open");

    setTimeout(() => {
      dispatch(closeModal());
    }, 330);
  };

  return (
    <Modal
      isOpen={isOpen}
      contentLabel="VideoModal"
      onRequestClose={closeVideo}
      shouldFocusAfterRender={false}
      style={customStyles}
      overlayClassName="video-modal-overlay"
      className="row video-modal"
      id="video-modal"
    >
      <video
        src={singleSlug}
        autoPlay={true}
        loop={true}
        controls={true}
        className="p-0"
      ></video>

      <button
        title="Close (Esc)"
        type="button"
        className="mfp-close"
        onClick={() => dispatch(closeModal())}
      >
        <span>×</span>
      </button>
    </Modal>
  );
}

export default VideoModal;
