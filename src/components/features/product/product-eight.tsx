import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { connect } from "react-redux";

import ALink from "@/components/features/custom-link";

import { toggleWishlist } from "@/store/wishlist";

import { toDecimal } from "@/utils";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import { toast } from "react-toastify";
import CartPopup from "./common/cart-popup";
import { openQuickview } from "@/store/modal";
import useShop from "@/hooks/useShop";

function ProductEight(props) {
  const { product, adClass } = props;

  const { data: wishlist = [] } = useAppSelector((state) => state.wishlist);
  const { user, isLogged } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  const { addToCart } = useShop();

  // decide if the product is wishlisted
  const isWishlisted =
    wishlist.findIndex((item) => item.slug === product.slug) > -1
      ? true
      : false;

  const showQuickviewHandler = (e) => {
    e.preventDefault();
    dispatch(openQuickview(product.slug));
  };

  const wishlistHandler = (e) => {
    dispatch(toggleWishlist(product));

    e.preventDefault();
    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setTimeout(() => {
      currentTarget.classList.remove("load-more-overlay", "loading");
    }, 1000);
  };

  const addToCartHandler = async (e) => {
    e.preventDefault();

    await addToCart(product, 1);
  };

  function renderPriceContent() {
    if (!isLogged) {
      return;
    }

    if (user?.pessoa.cliente) {
      return (
        <div className="product-price">
          <ins className="new-price">R$ {toDecimal(product.price)}</ins>
          {/* {product.price[1] && (
              <del className="old-price">R$ {toDecimal(product.price[1])}</del>
            )} */}
        </div>
      );
    }

    return (
      <div className="product-price">
        <ins className="new-price">R$ {toDecimal(product.minPrice)}</ins>
        {product.maxPrice && (
          <del className="new-price"> - R$ {toDecimal(product.maxPrice)}</del>
        )}
      </div>
    );
  }

  return (
    <div
      className={`product product-list ${adClass} ${
        product.variants.length > 0 ? "product-variable" : ""
      }`}
    >
      <figure className="product-media">
        <ALink href={`/p/${product.slug}`}>
          <LazyLoadImage
            alt="product"
            src={product.pictures[0].url}
            threshold={500}
            effect="opacity"
            width="300"
            height="338"
          />

          {product.pictures.length >= 2 ? (
            <LazyLoadImage
              alt="product"
              src={product.pictures[1].url}
              threshold={500}
              width="300"
              height="338"
              effect="opacity"
              wrapperClassName="product-image-hover"
            />
          ) : (
            ""
          )}
        </ALink>

        <div className="product-label-group">
          {product.is_new ? (
            <label className="product-label label-new">New</label>
          ) : (
            ""
          )}
          {product.is_top ? (
            <label className="product-label label-top">Top</label>
          ) : (
            ""
          )}
          {product.discount > 0 ? (
            product.variants.length === 0 ? (
              <label className="product-label label-sale">
                {product.discount}% OFF
              </label>
            ) : (
              <label className="product-label label-sale">Sale</label>
            )
          ) : (
            ""
          )}
        </div>
      </figure>

      <div className="product-details">
        <div className="product-cat">
          {product.categories
            ? product.categories.map((item, index) => (
                <React.Fragment key={item.name + "-" + index}>
                  <ALink
                    href={{ pathname: "/shop", query: { category: item.slug } }}
                  >
                    {item.name}
                    {index < product.categories.length - 1 ? ", " : ""}
                  </ALink>
                </React.Fragment>
              ))
            : ""}
        </div>

        <h3 className="product-name">
          <ALink href={`/product/default/${product.slug}`}>
            {product.name}
          </ALink>
        </h3>

        {renderPriceContent()}

        {/* <div className="ratings-container">
          <div className="ratings-full">
            <span
              className="ratings"
              style={{ width: 20 * product.ratings + "%" }}
            ></span>
            <span className="tooltiptext tooltip-top">
              {toDecimal(product.ratings)}
            </span>
          </div>

          <ALink
            href={`/product/default/${product.slug}`}
            className="rating-reviews"
          >
            ( {product.reviews} reviews )
          </ALink>
        </div> */}

        <p className="product-short-desc">{product.short_description}</p>

        <div className="product-action">
          {product.variants.length > 0 ? (
            <ALink
              href={`/product/default/${product.slug}`}
              className="btn-product btn-cart"
              title="Ir para o produto"
            >
              <span>{isLogged ? "Selecionar Opções" : "Ver Opções"}</span>
            </ALink>
          ) : !isLogged ? (
            <a
              href="#"
              className="btn-product btn-cart"
              title="Adicionar ao Carrinho"
              onClick={addToCartHandler}
            >
              <span>Ver Produto</span>
            </a>
          ) : (
            <a
              href="#"
              className="btn-product btn-cart"
              title="Adicionar ao Carrinho"
              onClick={addToCartHandler}
            >
              <i className="d-icon-bag"></i>
              <span>Adicionar ao Carrinho</span>
            </a>
          )}
          <a
            href="#"
            className="btn-product-icon btn-wishlist"
            title={isWishlisted ? "Remove from wishlist" : "Add to wishlist"}
            onClick={wishlistHandler}
          >
            <i
              className={isWishlisted ? "d-icon-heart-full" : "d-icon-heart"}
            ></i>
          </a>

          <ALink
            href="#"
            className="btn-product-icon btn-quickview"
            title="Quick View"
            onClick={showQuickviewHandler}
          >
            <i className="d-icon-search"></i>
          </ALink>
        </div>
      </div>
    </div>
  );
}

export default ProductEight;
