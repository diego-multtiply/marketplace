import { useEffect, useMemo, useState } from "react";

import { Magnifier } from "react-image-magnifiers";
import Modal from "react-modal";
import imagesLoaded from "imagesloaded";

import OwlCarousel from "@/components/features/owl-carousel";

import DetailOne from "@/components/partials/product/detail/detail-one";

import { mainSlider3 } from "@/utils/data/carousel";
import { closeQuickview } from "@/store/modal/index";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import { getProduct } from "@/server/services/products";
import { IProduct } from "@/common/types/products";
import useShop from "@/hooks/useShop";

const customStyles = {
  content: {
    position: "relative",
  },
  overlay: {
    background: "rgba(0,0,0,.4)",
    zIndex: "10000",
    overflowX: "hidden",
    overflowY: "auto",
  },
};

Modal.setAppElement("#__next");

function Quickview() {
  const [product, setProduct] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const { singleSlug: slug, quickview: isOpen } = useAppSelector(
    (state) => state.modal
  );
  const dispatch = useAppDispatch();

  const [loaded, setLoadingState] = useState(false);

  useEffect(() => {
    async function loadProduct() {
      setIsLoading(true);

      try {
        const response = await getProduct(slug);

        const data: IProduct = response.data.data;

        setProduct({
          ...data,
          name: data.nome,
          slug: data.slug,
          sku: data.sku,
          price: data.preco,
          minPrice: Number(data.preco_minimo),
          maxPrice: Number(data.preco_maximo),
          ratings: 5,
          reviews: 1,
          stock: 80,
          short_description: data.descricao,
          is_featured: false,
          is_new: false,
          is_sale: false,
          is_review: false,
          until: null,
          discount: 0,
          pictures: data.imagens.map((image) => ({
            url:
              "https://cyhnwguzha.cloudimg.io/" +
              image.url +
              "?width=300&height=338",
            width: 300,
            height: 338,
          })),
          small_pictures: null,
          large_pictures: data.imagens.map((image) => ({
            url:
              "https://cyhnwguzha.cloudimg.io/" +
              image.url +
              "?width=300&height=338",
            width: 300,
            height: 338,
          })),
          categories: [
            {
              name: data.descricao_departamento,
              slug: data.slug_departamento,
            },
            {
              name: data.descricao_grupo_principal,
              slug: data.slug_grupo_principal,
            },
            {
              name: data.descricao_grupo_secundario,
              slug: data.slug_grupo_secundario,
            },
          ],
          variants: data.variacoes.map((item) => ({
            price: 0,
            sale_price: null,
            color: item.cor
              ? {
                  name: item.cor,
                  slug: item.cor,
                  color: "#fff",
                }
              : null,
            size: item.tamanho
              ? {
                  name: item.tamanho,
                  slug: item.tamanho,
                  size: item.tamanho,
                }
              : item.numeracao
              ? {
                  name: item.numeracao,
                  slug: item.numeracao,
                  size: item.numeracao,
                }
              : null,
          })),
          quantity: data.volumes,
          un: data.unidade,
        });
        setIsLoading(false);
      } catch (err) {
      } finally {
        setIsLoading(false);
      }
    }

    if (isOpen) {
      loadProduct();
    }
  }, [slug, isOpen]);

  useEffect(() => {
    setTimeout(() => {
      if (
        !isLoading &&
        product &&
        isOpen &&
        document.querySelector(".quickview-modal")
      )
        imagesLoaded(".quickview-modal")
          .on("done", function () {
            setLoadingState(true);
            window
              .jQuery(".quickview-modal .product-single-carousel")
              .trigger("refresh.owl.carousel");
          })
          .on("progress", function () {
            setLoadingState(false);
          });
    }, 200);
  }, [product, isOpen, isLoading]);

  if (!isOpen) return <div></div>;

  if (slug === "" || !product) return "";

  const closeQuick = () => {
    document.querySelector(".ReactModal__Overlay")?.classList.add("removed");
    document.querySelector(".quickview-modal")?.classList.add("removed");
    setLoadingState(false);
    setTimeout(() => {
      dispatch(closeQuickview());
    }, 330);
  };

  return (
    <Modal
      isOpen={isOpen}
      contentLabel="QuickView"
      onRequestClose={closeQuick}
      shouldFocusAfterRender={false}
      style={customStyles}
      className="product product-single row product-popup quickview-modal"
      id="product-quickview"
    >
      <>
        <div className={`row p-0 m-0 ${loaded ? "" : "d-none"}`}>
          <div className="col-md-6">
            <div className="product-gallery mb-md-0 pb-0">
              <div className="product-label-group">
                {product.is_new ? (
                  <label className="product-label label-new">New</label>
                ) : (
                  ""
                )}
                {product.is_top ? (
                  <label className="product-label label-top">Top</label>
                ) : (
                  ""
                )}
                {product.discount > 0 ? (
                  product.variants.length === 0 ? (
                    <label className="product-label label-sale">
                      {product.discount}% OFF
                    </label>
                  ) : (
                    <label className="product-label label-sale">Promoção</label>
                  )
                ) : (
                  ""
                )}
              </div>

              <OwlCarousel
                adClass="product-single-carousel owl-theme owl-nav-inner"
                options={mainSlider3}
              >
                {product &&
                  product &&
                  product?.large_pictures?.map((item, index) => (
                    <Magnifier
                      key={"quickview-image-" + index}
                      imageSrc={item.url}
                      imageAlt="magnifier"
                      largeImageSrc={item.url}
                      dragToMove={false}
                      mouseActivation="hover"
                      cursorStyleActive="crosshair"
                      className="product-image large-image"
                    />
                  ))}
              </OwlCarousel>
            </div>
          </div>

          <div className="col-md-6">
            <DetailOne data={product} adClass="scrollable pr-3" isNav={false} />
          </div>
        </div>

        <button
          title="Close (Esc)"
          type="button"
          className="mfp-close p-0"
          onClick={closeQuick}
        >
          <span>×</span>
        </button>
      </>
      {!loaded && (
        <div className="product row p-0 m-0 skeleton-body mfp-product">
          <div className="col-md-6">
            <div className="skel-pro-gallery"></div>
          </div>

          <div className="col-md-6">
            <div className="skel-pro-summary"></div>
          </div>
        </div>
      )}
    </Modal>
  );
}

export default Quickview;
