import React from "react";

export default function AlertPopup({ message }: { message: string }) {
  return (
    <div className="minipopup-area-cart-error">
      <div className="minipopup-box show" style={{ top: "0" }}>
        <div className="d-flex align-items-center">
          <i
            className="d-icon-alert"
            style={{
              fontSize: 24,
              color: "#d26e4b",
            }}
          ></i>
          <p className="minipopup-title mb-0 ml-2" style={{ color: "#d26e4b" }}>
            Atenção!
          </p>
        </div>

        <p className="mb-0 pt-2">{message}</p>

        {/* <div className="action-group d-flex">
          <ALink
            href="/pages/cart"
            className="btn btn-sm btn-outline btn-primary btn-rounded w-100"
          >
            Ver Carrinho
          </ALink>
        </div> */}
      </div>
    </div>
  );
}
