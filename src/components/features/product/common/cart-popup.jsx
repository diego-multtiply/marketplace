import React from "react";
import ALink from "@/components/features/custom-link";

import { toDecimal } from "@/utils";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useAppSelector } from "@/hooks/useStore";

export default function CartPopup(props) {
  const { product } = props;

  const { user } = useAppSelector((state) => state.auth);

  return (
    <div className="minipopup-area">
      <div className="minipopup-box show" style={{ top: "0" }}>
        <p className="minipopup-title">Adicionado com sucesso</p>

        <div className="product product-purchased  product-cart mb-0">
          <figure className="product-media pure-media">
            <ALink href={`/p/${product.slug}`}>
              <LazyLoadImage
                alt="product"
                src={product.pictures[0].url}
                threshold={500}
                effect="opacity"
                width="90"
                height="90"
              />
            </ALink>
          </figure>
          <div className="product-detail">
            <ALink href={`/´p/${product.slug}`} className="product-name">
              {product.name}
            </ALink>
            <div className="product-meta">
              QTDE.: <span className="product-sku">{product?.qty}</span>
              <br />
              QTDE {product?.un}:{" "}
              <span className="product-sku">{product?.quantity}</span>
              <br />
              QTDE. TOTAL ITENS:{" "}
              <span className="product-sku">
                {product?.quantity * product?.qty}
              </span>
            </div>
            <span className="price-box">
              <span className="product-quantity">{product.qty}</span>
              <span className="product-price">
                {user.pessoa.cliente
                  ? `R$ ${toDecimal(
                      (product?.price || product?.preco) *
                        product.quantity *
                        product.qty
                    )}`
                  : `R$ ${toDecimal(
                      product.minPrice * product.quantity * product.qty
                    )}
                - R$ ${toDecimal(
                  product.maxPrice * product.quantity * product.qty
                )}`}
              </span>
            </span>
          </div>
        </div>

        <div className="action-group d-flex">
          <ALink
            href="/pages/cart"
            className="btn btn-sm btn-outline btn-primary btn-rounded w-100"
          >
            Ver Carrinho
          </ALink>
        </div>
      </div>
    </div>
  );
}
