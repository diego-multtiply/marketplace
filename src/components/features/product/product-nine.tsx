import React, { useMemo } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { connect } from "react-redux";

import ALink from "@/components/features/custom-link";

import { toDecimal } from "@/utils";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import { openQuickview } from "@/store/modal";
import { toggleWishlist } from "@/store/wishlist";
import useShop from "@/hooks/useShop";

function ProductOne(props) {
  const { product, adClass, isCategory = false } = props;

  const { data: wishlist = [] } = useAppSelector((state) => state.wishlist);
  const { user, isLogged } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  const { addToCart } = useShop();

  // decide if the product is wishlisted
  let isWishlisted =
    wishlist.findIndex((item) => item?.slug === product.slug) > -1
      ? true
      : false;

  const showQuickviewHandler = () => {
    dispatch(openQuickview(product.slug));
  };

  const wishlistHandler = (e) => {
    dispatch(toggleWishlist(product));

    e.preventDefault();
    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setTimeout(() => {
      currentTarget.classList.remove("load-more-overlay", "loading");
    }, 1000);
  };

  const addToCartHandler = async (e) => {
    e.preventDefault();

    await addToCart(product, 1);
  };

  function renderPriceContent() {
    if (!isLogged) {
      return (
        <ALink
          href="/pages/login"
          style={{
            fontWeight: 700,
          }}
        >
          Faça login para Comprar
        </ALink>
      );
    }

    if (user?.pessoa.cliente) {
      return (
        <div className="product-price">
          <ins className="new-price">R$ {toDecimal(product.price)}</ins>
          {/* {product.price[1] && (
              <del className="old-price">R$ {toDecimal(product.price[1])}</del>
            )} */}
        </div>
      );
    }

    return (
      <div className="product-price">
        <ins className="new-price">R$ {toDecimal(product.minPrice)}</ins>
        {product.maxPrice && (
          <del className="new-price"> - R$ {toDecimal(product.maxPrice)}</del>
        )}
      </div>
    );
  }

  return (
    <div className={`product product-border text-center ${adClass}`}>
      <figure className="product-media">
        <ALink href={`/p/${product.slug}`}>
          <LazyLoadImage
            alt={product.name}
            src={product.pictures[0].url}
            threshold={500}
            effect="opacity"
            width="300"
            height="338"
          />

          {product.pictures.length >= 2 ? (
            <LazyLoadImage
              alt={product.name}
              src={product.pictures[1].url}
              threshold={500}
              effect="opacity"
              width="300"
              height="338"
            />
          ) : (
            ""
          )}
        </ALink>

        <div className="product-label-group">
          {product.is_new && (
            <label className="product-label label-new">Novo</label>
          )}
          {product.is_top && (
            <label className="product-label label-top">Top</label>
          )}
          {product.discount > 0 && (
            <label className="product-label label-sale">
              {product.discount}% OFF
            </label>
          )}
        </div>

        <div className="product-action-vertical">
          {isLogged && (
            <a
              href="javascript: void(0)"
              className="btn-product-icon btn-cart"
              title="Adicionar ao carrinho"
              onClick={addToCartHandler}
            >
              <i className="d-icon-bag"></i>
            </a>
          )}
          <a
            href="javascript: void(0)"
            className={`btn-product-icon btn-wishlist`}
            title={
              isWishlisted ? "Remover dos favoritos" : "Adicionar aos favoritos"
            }
            onClick={wishlistHandler}
          >
            <i
              className={isWishlisted ? "d-icon-heart-full" : "d-icon-heart"}
            ></i>
          </a>
          <ALink
            href="javascript: void(0)"
            className="btn-product-icon btn-quickview"
            title="Quick View"
            onClick={showQuickviewHandler}
          >
            <i className="d-icon-search"></i>
          </ALink>
        </div>
      </figure>

      <div className="product-details">
        {isCategory && (
          <div className="product-cat">
            {product.categories
              ? product.categories.map((item, index) => (
                  <React.Fragment key={item.name + "-" + index}>
                    <ALink
                      href={{
                        pathname: "/shop",
                        query: { category: item.slug },
                      }}
                    >
                      {item.name}
                      {index < product.categories.length - 1 ? ", " : ""}
                    </ALink>
                  </React.Fragment>
                ))
              : ""}
          </div>
        )}

        <h3 className="product-name">
          <ALink href={`/product/default/${product.slug}`}>
            {product.name}
          </ALink>
        </h3>

        {renderPriceContent()}

        {product.is_review && (
          <div className="ratings-container">
            <div className="ratings-full">
              <span
                className="ratings"
                style={{ width: 20 * product.ratings + "%" }}
              ></span>
              <span className="tooltiptext tooltip-top">
                {toDecimal(product.ratings)}
              </span>
            </div>

            <ALink
              href={`/product/default/${product.slug}`}
              className="rating-reviews"
            >
              ( {product.reviews} reviews )
            </ALink>
          </div>
        )}
      </div>
    </div>
  );
}

export default ProductOne;
