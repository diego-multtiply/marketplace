import { useEffect, useMemo } from "react";
import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { connect } from "react-redux";

import ALink from "@/components/features/custom-link";

import { toDecimal } from "@/utils";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import { toggleWishlist } from "@/store/wishlist";
import { openQuickview } from "@/store/modal";
import { addToCart } from "@/store/cart";
import useShop from "@/hooks/useShop";

function ProductFour(props) {
  const { product, adClass } = props;

  const { data: wishlist = [] } = useAppSelector((state) => state.wishlist);
  const { user, isLogged } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  // decide if the product is wishlisted
  let isWishlisted;
  isWishlisted =
    wishlist.findIndex((item) => item.slug === product.slug) > -1
      ? true
      : false;

  useEffect(() => {
    let items = document.querySelectorAll(".product-slideup-content");

    for (let i = 0; i < items.length; i++) {
      items[i].addEventListener("mouseenter", mouseOverHandler, false);
      items[i].addEventListener("touchstart", mouseOverHandler, false);
      items[i].addEventListener("mouseleave", mouseLeaveHandler, false);
      items[i].addEventListener("touchleave", mouseLeaveHandler, false);
    }

    return () => {
      for (let i = 0; i < items.length; i++) {
        items[i].removeEventListener("mouseenter", mouseOverHandler);
        items[i].removeEventListener("touchstart", mouseOverHandler);
        items[i].removeEventListener("mouseleave", mouseLeaveHandler);
        items[i].removeEventListener("touchleave", mouseLeaveHandler);
      }
    };
  }, []);

  const showQuickviewHandler = () => {
    dispatch(openQuickview(product.slug));
  };

  const wishlistHandler = (e) => {
    dispatch(toggleWishlist(product));

    e.preventDefault();
    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setTimeout(() => {
      currentTarget.classList.remove("load-more-overlay", "loading");
    }, 1000);
  };

  const { addToCart } = useShop();

  const addToCartHandler = async (e) => {
    e.preventDefault();

    await addToCart(product, 1);
  };

  const mouseOverHandler = (e) => {
    let height = e.currentTarget.querySelector(
      ".product-hide-details"
    ).offsetHeight;
    e.currentTarget
      .querySelector(".product-details")
      .setAttribute("style", `transform: translateY(-${height}px)`);
    e.currentTarget
      .querySelector(".product-hide-details")
      .setAttribute("style", `transform: translateY(-${height}px)`);
  };

  const mouseLeaveHandler = (e) => {
    e.currentTarget.querySelector(".product-details").setAttribute("style", "");
    e.currentTarget
      .querySelector(".product-hide-details")
      .setAttribute("style", "");
  };

  function renderPriceContent() {
    if (!isLogged) {
      return;
    }

    if (user?.pessoa.cliente) {
      return (
        <div className="product-price">
          <ins className="new-price">R$ {toDecimal(product.price)}</ins>
          {/* {product.price[1] && (
              <del className="old-price">R$ {toDecimal(product.price[1])}</del>
            )} */}
        </div>
      );
    }

    return (
      <div className="product-price">
        <ins className="new-price">R$ {toDecimal(product.minPrice)}</ins>
        {product.maxPrice && (
          <del className="new-price"> - R$ {toDecimal(product.maxPrice)}</del>
        )}
      </div>
    );
  }

  return (
    <div
      className={`product product-slideup-content text-center ${adClass} ${
        product.variants.length > 0 ? "product-variable" : ""
      }`}
    >
      <figure className="product-media">
        <ALink href={`/p/${product.slug}`}>
          <LazyLoadImage
            alt="product"
            src={product.pictures[0].url}
            threshold={500}
            effect="opacity"
            width="300"
            height="338"
          />

          {product.pictures.length >= 2 ? (
            <LazyLoadImage
              alt="product"
              src={product.pictures[1].url}
              threshold={500}
              width="300"
              height="338"
              effect="opacity"
              wrapperClassName="product-image-hover"
            />
          ) : (
            ""
          )}
        </ALink>

        <div className="product-label-group">
          {product.is_new && (
            <label className="product-label label-new">Novo</label>
          )}
          {product.is_top && (
            <label className="product-label label-top">Top</label>
          )}
          {product.discount > 0 && (
            <label className="product-label label-sale">
              {product.discount}% OFF
            </label>
          )}
        </div>
      </figure>

      <div className="product-details">
        <div className="product-cat">
          {product.categories
            ? product.categories.map((item, index) => (
                <React.Fragment key={item.name + "-" + index}>
                  <ALink
                    href={{ pathname: "/shop", query: { category: item.slug } }}
                  >
                    {item.name}
                    {index < product.categories.length - 1 ? ", " : ""}
                  </ALink>
                </React.Fragment>
              ))
            : ""}
        </div>

        <h3 className="product-name">
          <ALink href={`/p/${product.slug}`}>{product.name}</ALink>
        </h3>

        {renderPriceContent()}
      </div>

      <div className="product-hide-details">
        {product?.is_review && (
          <div className="ratings-container">
            <div className="ratings-full">
              <span
                className="ratings"
                style={{ width: 20 * product.ratings + "%" }}
              ></span>
              <span className="tooltiptext tooltip-top">
                {toDecimal(product.ratings)}
              </span>
            </div>

            <ALink
              href={`/product/default/${product.slug}`}
              className="rating-reviews"
            >
              ( {product.reviews} reviews )
            </ALink>
          </div>
        )}

        <div className="product-action">
          <a
            href="javascript: void(0);"
            className="btn-product-icon btn-wishlist"
            title={
              isWishlisted ? "Remover dos favoritos" : "Adicionar aos favoritos"
            }
            onClick={wishlistHandler}
          >
            <i
              className={isWishlisted ? "d-icon-heart-full" : "d-icon-heart"}
            ></i>
          </a>
          {product.variants.length > 0 ? (
            <ALink
              href={`/p/${product.slug}`}
              className="btn-product btn-cart"
              title="Ir para o produto"
            >
              <span>{isLogged ? "Selecionar Opções" : "Ver Opções"}</span>
            </ALink>
          ) : isLogged ? (
            <a
              href="#"
              className="btn-product btn-cart"
              title="Adicionar ao Carrinho"
              onClick={addToCartHandler}
            >
              <i className="d-icon-bag"></i>
              <span>Adicionar ao Carrinho</span>
            </a>
          ) : null}
          <ALink
            href="javascript: void(0)"
            className="btn-product-icon btn-quickview"
            title="Visualização rápida"
            onClick={showQuickviewHandler}
          >
            <i className="d-icon-search"></i>
          </ALink>
        </div>
      </div>
    </div>
  );
}

export default ProductFour;
