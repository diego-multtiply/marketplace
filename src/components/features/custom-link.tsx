import { parseContent } from "@/utils";
import Link from "next/link";

export default function ALink(props) {
  const preventDefault = (e) => {
    if (props.href === "#") {
      e.preventDefault();
    }

    if (props.onClick) {
      props.onClick();
    }
  };

  return props.content ? (
    <Link
      className={props.className}
      style={props.style}
      onClick={preventDefault}
      dangerouslySetInnerHTML={parseContent(props.content)}
      {...props}
    >
      {props.children}
    </Link>
  ) : (
    <Link
      className={props.className}
      style={props.style}
      onClick={preventDefault}
      {...props}
    >
      {props.children}
    </Link>
  );
}
