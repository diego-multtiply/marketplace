import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import ToolBox from "@/components/partials/shop/toolbox";
import ProductNine from "@/components/features/product/product-nine";
import ProductEight from "@/components/features/product/product-eight";

import Pagination from "@/components/features/pagination";
import { getAllProducts } from "@/server/services/products";
import { IProduct } from "@/common/types/products";
import useShop from "@/hooks/useShop";
import ProductFour from "@/components/features/product/product-four";
import ProductOne from "@/components/features/product/product-nine";
import ProductTwo from "@/components/features/product/product-two";
import ProductThree from "@/components/features/product/product-three";
import ProductFive from "@/components/features/product/product-five";
import ProductSix from "@/components/features/product/product-six";

function ProductListOne(props) {
  const {
    itemsPerRow = 3,
    type = "left",
    isToolbox = true,
    adClass = "",
  } = props;

  const { query, isReady } = useRouter();
  const [products, setProducts] = useState<IProduct[]>([]);
  const [loading, setLoading] = useState(false);
  const [pagination, setPagination] = useState(null);
  const [totalPage, setTotalPage] = useState("15");

  const { getProductPrice } = useShop();

  const gridClasses = {
    3: "cols-2 cols-sm-3",
    4: "cols-2 cols-sm-3 cols-md-4",
    5: "cols-2 cols-sm-3 cols-md-4 cols-xl-5",
    6: "cols-2 cols-sm-3 cols-md-4 cols-xl-6",
    7: "cols-2 cols-sm-3 cols-md-4 cols-lg-5 cols-xl-7",
    8: "cols-2 cols-sm-3 cols-md-4 cols-lg-5 cols-xl-8",
  };
  const perPage = query.per_page ? parseInt(query.per_page) : 15;

  const page = query.page ? query.page : 1;
  const gridType = query.type ? query.type : "grid";

  useEffect(() => {
    async function loadProducts() {
      setLoading(true);
      const { data } = await getAllProducts(
        String(page),
        String(perPage),
        query
      );

      const requestData = data?.data;

      const pagination = requestData?.meta;

      setPagination(pagination);
      setTotalPage(pagination?.last_page_url.replace("/?page="));

      setProducts(
        requestData?.data.map((data: IProduct) => ({
          ...data,
          id: data.id,
          slug: data.slug,
          name: data.nome,
          // price: [getProductPrice(data)],
          price: data.preco,
          minPrice: Number(data.preco_minimo),
          maxPrice: Number(data.preco_maximo),
          discount: 0,
          short_description: data.descricao,
          complementar_description: data.descricao_complementar,
          collection: data.descricao_colecao,
          sku: data.sku,
          stock: 80,
          ratings: 5,
          reviews: 1,
          sale_count: 30,
          is_featured: true,
          is_new: false,
          is_sale: false,
          is_review: false,
          is_top: false,
          until: null,
          dimensions: [
            {
              name: "Altura",
              value: data.altura,
            },
            {
              name: "Largura",
              value: data.largura,
            },
            {
              name: "Comprimento",
              value: data?.comprimento,
            },
          ],
          pictures: data.imagens.map((image) => ({
            url:
              "https://cyhnwguzha.cloudimg.io/" +
              image.url +
              "?width=300&height=338",
            width: 300,
            height: 338,
          })),
          small_pictures: null,
          large_pictures: data.imagens.map((image) => ({
            url:
              "https://cyhnwguzha.cloudimg.io/" +
              image.url +
              "?width=800&height=900",
            width: 800,
            height: 900,
          })),
          categories: [
            {
              name: data.descricao_departamento,
              slug: data.slug_departamento,
            },
            {
              name: data.descricao_grupo_principal,
              slug: data.slug_grupo_principal,
            },
            {
              name: data.descricao_grupo_secundario,
              slug: data.slug_grupo_secundario,
            },
          ],
          tags: [],
          brands: [
            {
              name: data.marca,
            },
          ],
          variants: data.variacoes.map((item) => ({
            price: 0,
            sale_price: null,
            color: item.cor
              ? {
                  name: item.cor,
                  slug: item.cor,
                  color: "#fff",
                }
              : null,
            size: item.tamanho
              ? {
                  name: item.tamanho,
                  slug: item.tamanho,
                  size: item.tamanho,
                }
              : item.numeracao
              ? {
                  name: item.numeracao,
                  slug: item.numeracao,
                  size: item.numeracao,
                }
              : null,
          })),
          quantity: data.volumes,
          un: data.unidade,
        }))
      );

      setLoading(false);
    }

    if (isReady) {
      loadProducts();
    }
  }, [query, isReady]);

  return (
    <>
      {isToolbox ? <ToolBox type={type} /> : ""}
      {loading ? (
        gridType === "grid" ? (
          <div
            className={`row product-wrapper ${gridClasses[itemsPerRow]} ${adClass}`}
          >
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((item) => (
              <div
                className="product-loading-overlay"
                key={"popup-skel-" + item}
              ></div>
            ))}
          </div>
        ) : (
          <div
            className={`row product-wrapper skeleton-body cols-1 ${adClass}`}
          >
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((item) => (
              <div
                className="skel-pro skel-pro-list mb-4"
                key={"list-skel-" + item}
              ></div>
            ))}
          </div>
        )
      ) : (
        ""
      )}
      {gridType === "grid" ? (
        <div
          className={`row product-wrapper ${gridClasses[itemsPerRow]} ${adClass}`}
        >
          {products &&
            products.map((item) => (
              <div className="product-wrap" key={"shop-" + item.slug}>
                {item.variants.length > 0 ? (
                  <ProductFour product={item} adClass="" />
                ) : (
                  <ProductFive product={item} adClass="" />
                )}
              </div>
            ))}
        </div>
      ) : (
        <div className={`product-lists product-wrapper ${adClass}`}>
          {products &&
            products.map((item) => (
              <ProductEight product={item} key={"shop-list-" + item.slug} />
            ))}
        </div>
      )}

      {products && products.length === 0 ? (
        <p className="ml-1">
          Nenhum produto foi encontrado com o filtro selecionado.
        </p>
      ) : (
        ""
      )}

      {products && pagination?.total > 0 && (
        <div className="toolbox toolbox-pagination">
          {products && (
            <p className="show-info">
              Mostrando{" "}
              <span>
                {perPage * (page - 1) + 1} -{" "}
                {Math.min(perPage * page, pagination?.total)} de{" "}
                {pagination?.total}
              </span>
              Produtos
            </p>
          )}

          <Pagination totalPage={pagination.last_page} />
        </div>
      )}
    </>
  );
}

export default ProductListOne;
