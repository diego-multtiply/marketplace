import { useCallback, useEffect, useState } from "react";
import { useRouter } from "next/router";
import InputRange from "react-input-range";
import SlideToggle from "react-slide-toggle";
import _ from "lodash";

import ALink from "@/components/features/custom-link";
import Card from "@/components/features/accordion/card";

import { scrollTopHandler } from "@/utils";
import { useAppSelector } from "@/hooks/useStore";
import { IDepartamento, IGrupoPrincipal } from "@/store/shop";
import { getFilter } from "@/server/services/shop";
import Collapse from "react-bootstrap/Collapse";

function SidebarFilterOne(props) {
  const [isLoading, setIsLoading] = useState(false);
  const [fornecedores, setFornecedores] = useState([]);
  const [representantes, setRepresentantes] = useState([]);
  const [fornecedoresSearch, setFornecedoresSearch] = useState("");
  const [representantesSearch, setRepresentantesSearch] = useState("");
  const [fornecedoresSelected, setFornecedoresSelected] = useState([]);
  const [representantesSelected, setRepresentantesSelected] = useState([]);

  const { departamentos } = useAppSelector((state) => state.shop);
  const { user, isLogged } = useAppSelector((state) => state.auth);

  const { type = "left", isFeatured = false } = props;
  const router = useRouter();

  const query = router.query;

  let tmpPrice = {
    max: query.maxPrice ? parseInt(query.maxPrice) : 200,
    min: query.minPrice ? parseInt(query.minPrice) : 0,
  };
  const [filterPrice, setPrice] = useState(tmpPrice);
  const [isFirst, setFirst] = useState(true);
  let timerId;

  useEffect(() => {
    window.addEventListener("resize", hideSidebar);

    return () => {
      window.removeEventListener("resize", hideSidebar);
    };
  }, []);

  useEffect(() => {
    setPrice({
      max: query.maxPrice ? parseInt(query.maxPrice) : 200,
      min: query.minPrice ? parseInt(query.minPrice) : 0,
    });
    if (isFirst) {
      setFirst(false);
    } else {
      scrollTopHandler();
    }
  }, [query]);

  async function loadSideBar({
    fornecedor,
    representante,
  }: {
    fornecedor?: string | null;
    representante?: string | null;
  }) {
    try {
      const { data } = await getFilter({
        provider:
          fornecedor !== null && fornecedor !== undefined
            ? fornecedor
            : fornecedoresSearch,
        representative:
          representante !== null && representante !== undefined
            ? representante
            : representantesSearch,
      });

      setFornecedores(data.data.fornecedores);
      setRepresentantes(data.data.representantes);
    } finally {
      stopLoadingFornecedores();
      stopLoadingRepresentantes();
    }
  }

  useEffect(() => {
    if (router.isReady) {
      loadSideBar({});
    }
  }, [router.isReady]);

  useEffect(() => {}, [fornecedoresSelected]);

  const debounceFornecedores = useCallback(
    _.debounce(
      (v) =>
        loadSideBar({ fornecedor: v, representante: representantesSearch }),
      1000
    ),
    []
  );

  const debounceRepresentantes = useCallback(
    _.debounce(
      (v) => loadSideBar({ fornecedor: fornecedoresSearch, representante: v }),
      1000
    ),
    []
  );

  const filterByPrice = (e) => {
    e.preventDefault();
    let url = router.pathname.replace("[grid]", query.grid);
    let arr = [
      `minPrice=${filterPrice.min}`,
      `maxPrice=${filterPrice.max}`,
      "page=1",
    ];
    for (let key in query) {
      if (
        key !== "minPrice" &&
        key !== "maxPrice" &&
        key !== "page" &&
        key !== "grid"
      )
        arr.push(key + "=" + query[key]);
    }
    url = url + "?" + arr.join("&");
    router.push(url);
  };

  const onChangePrice = (value) => {
    setPrice(value);
  };

  const toggleSidebar = (e) => {
    e.preventDefault();
    document
      .querySelector("body")
      ?.classList.remove(
        `${
          type === "left" || type === "off-canvas"
            ? "sidebar-active"
            : "right-sidebar-active"
        }`
      );

    let stickyWraper = e.currentTarget.closest(".sticky-sidebar-wrapper");

    let mainContent = e.currentTarget.closest(".main-content-wrap");
    if (mainContent && type !== "off-canvas" && query.grid !== "4cols")
      mainContent.querySelector(".row.product-wrapper") &&
        mainContent
          .querySelector(".row.product-wrapper")
          .classList.toggle("cols-md-4");

    if (mainContent && stickyWraper) {
      stickyWraper.classList.toggle("closed");

      if (stickyWraper.classList.contains("closed")) {
        mainContent.classList.add("overflow-hidden");
        clearTimeout(timerId);
      } else {
        timerId = setTimeout(() => {
          mainContent.classList.remove("overflow-hidden");
        }, 500);
      }
    }
  };

  const showSidebar = (e) => {
    e.preventDefault();
    document.querySelector("body")?.classList.add("sidebar-active");
  };

  const hideSidebar = () => {
    document
      .querySelector("body")
      ?.classList.remove(
        `${
          type === "left" ||
          type === "off-canvas" ||
          type === "boxed" ||
          type === "banner"
            ? "sidebar-active"
            : "right-sidebar-active"
        }`
      );
  };

  function handleSelectFornecedor(checked: boolean, fornecedor) {
    const arr = [...fornecedoresSelected];

    const index = arr.findIndex((item) => item.id === fornecedor.id);

    if (index >= 0) {
      arr.splice(index, 1);
    } else {
      arr.push(fornecedor);
    }

    setFornecedoresSelected(arr);

    let url = router.pathname.replace("[grid]", query.grid);
    let arrQuery = [
      `provider=${arr.map((item) => item.fornecedor.id).join(":")}`,
      "page=1",
    ];
    for (let key in query) {
      if (key !== "provider" && key !== "page" && key !== "grid")
        arrQuery.push(key + "=" + query[key]);
    }
    url = url + "?" + arrQuery.join("&");
    router.push(url);
  }

  function handleSelectRepresentante(checked: boolean, representante) {
    const arr = [...representantesSelected];

    const index = arr.findIndex((item) => item.id === representante.id);

    if (index >= 0) {
      arr.splice(index, 1);
    } else {
      arr.push(representante);
    }

    setRepresentantesSelected(arr);

    let url = router.pathname.replace("[grid]", query.grid);
    let arrQuery = [
      `representative=${arr.map((item) => item.representante.id).join(":")}`,
      "page=1",
    ];
    for (let key in query) {
      if (key !== "representative" && key !== "page" && key !== "grid")
        arrQuery.push(key + "=" + query[key]);
    }
    url = url + "?" + arrQuery.join("&");
    router.push(url);
  }

  const renderGrupoSecundario = (item: IGrupoPrincipal) => {
    return item.grupoSecundario.map((grupoSecundarioItem, index) => (
      <li
        key={String(grupoSecundarioItem.id)}
        className={`with-ul ${
          String(grupoSecundarioItem.slug) === query.category ? "show" : ""
        } `}
      >
        <ALink
          scroll={false}
          href={{
            pathname: router.pathname,
            query: {
              ...router.query,
              category: grupoSecundarioItem.slug,
            },
          }}
        >
          {grupoSecundarioItem.descricao}
        </ALink>
      </li>
    ));
  };

  const renderGrupoPrincipal = (item: IDepartamento) => {
    return item.grupoPrincipal.map((grupoPrincipalItem, index) => (
      <li
        key={String(grupoPrincipalItem.id)}
        className={`with-ul overflow-hidden ${
          String(grupoPrincipalItem.slug) === query.category ? "show" : ""
        } `}
      >
        <SlideToggle collapsed={true}>
          {({ onToggle, setCollapsibleElement, toggleState }) => (
            <>
              <ALink
                href={{
                  pathname: router.pathname,
                  query: {
                    ...router.query,
                    category: grupoPrincipalItem.slug,
                  },
                }}
                scroll={false}
              >
                {grupoPrincipalItem.descricao}
                <i
                  className={`fas fa-chevron-down ${toggleState.toLowerCase()}`}
                  onClick={(e) => {
                    onToggle();
                    e.stopPropagation();
                    e.preventDefault();
                  }}
                ></i>
              </ALink>

              <div ref={setCollapsibleElement}>
                <div>
                  <ul style={{ display: "block" }}>
                    {renderGrupoSecundario(grupoPrincipalItem)}
                  </ul>
                </div>
              </div>
            </>
          )}
        </SlideToggle>
      </li>
    ));
  };

  const renderDepartamentos = () => {
    return departamentos?.map((item) => (
      <li
        key={String(item.id)}
        className={`with-ul overflow-hidden ${
          String(item.slug) === query.category ? "show" : ""
        } `}
      >
        <SlideToggle collapsed={true}>
          {({ onToggle, setCollapsibleElement, toggleState }) => (
            <>
              <ALink
                href={{
                  pathname: router.pathname,
                  query: {
                    ...router.query,
                    category: item.slug,
                  },
                }}
                scroll={false}
              >
                {item.descricao}
                <i
                  className={`fas fa-chevron-down ${toggleState.toLowerCase()}`}
                  onClick={(e) => {
                    onToggle();
                    e.stopPropagation();
                    e.preventDefault();
                  }}
                ></i>
              </ALink>

              <div ref={setCollapsibleElement}>
                <div>
                  <ul style={{ display: "block" }}>
                    {renderGrupoPrincipal(item)}
                  </ul>
                </div>
              </div>
            </>
          )}
        </SlideToggle>
      </li>
    ));
  };

  function startLoadingFornecedores() {
    const container = document.getElementById("fornecedores-container");

    if (container) {
      container.classList.add("load-more-overlay", "loading");
      container.style.backgroundColor = "rgba(0, 0, 0, 0.1)";
    }
  }

  function stopLoadingFornecedores() {
    const container = document.getElementById("fornecedores-container");

    if (container) {
      container.classList.remove("load-more-overlay", "loading");
      container.style.backgroundColor = "transparent";
    }
  }

  function startLoadingRepresentantes() {
    const container = document.getElementById("representantes-container");

    if (container) {
      container.classList.add("load-more-overlay", "loading");
      container.style.backgroundColor = "rgba(0, 0, 0, 0.1)";
    }
  }

  function stopLoadingRepresentantes() {
    const container = document.getElementById("representantes-container");

    if (container) {
      container.classList.remove("load-more-overlay", "loading");
      container.style.backgroundColor = "transparent";
    }
  }

  function handleRemoveSelectedFornecedor(index) {
    const arr = [...fornecedoresSelected];

    arr.splice(index, 1);

    setFornecedoresSelected(arr);

    let url = router.pathname.replace("[grid]", query.grid);
    let arrQuery = [
      `provider=${arr.map((item) => item.id).join(":")}`,
      "page=1",
    ];
    for (let key in query) {
      if (key !== "provider" && key !== "page" && key !== "grid")
        arrQuery.push(key + "=" + query[key]);
    }
    url = url + "?" + arrQuery.join("&");
    router.push(url);
  }

  function handleRemoveSelectedRepresentante(index) {
    const arr = [...representantesSelected];

    arr.splice(index, 1);

    setRepresentantesSelected(arr);

    let url = router.pathname.replace("[grid]", query.grid);
    let arrQuery = [
      `representative=${arr.map((item) => item.id).join(":")}`,
      "page=1",
    ];
    for (let key in query) {
      if (key !== "representative" && key !== "page" && key !== "grid")
        arrQuery.push(key + "=" + query[key]);
    }
    url = url + "?" + arrQuery.join("&");
    router.push(url);
  }

  return (
    <aside
      className={`col-lg-3 shop-sidebar skeleton-body ${
        type === "off-canvas" ? "" : "sidebar-fixed sticky-sidebar-wrapper"
      } ${
        type === "off-canvas" || type === "boxed" ? "" : "sidebar-toggle-remain"
      } ${
        type === "left" ||
        type === "off-canvas" ||
        type === "boxed" ||
        type === "banner"
          ? "sidebar"
          : "right-sidebar"
      }`}
    >
      <div className="sidebar-overlay" onClick={hideSidebar}></div>
      {type === "boxed" || type === "banner" ? (
        <a href="#" className="sidebar-toggle" onClick={showSidebar}>
          <i className="fas fa-chevron-right"></i>
        </a>
      ) : (
        ""
      )}
      <ALink className="sidebar-close" href="#" onClick={hideSidebar}>
        <i className="d-icon-times"></i>
      </ALink>

      <div className="sidebar-content">
        {!isLoading ? (
          <div className="sticky-sidebar">
            {type === "boxed" || type === "banner" ? (
              ""
            ) : (
              <div className="filter-actions mb-4">
                <a
                  href="#"
                  className="sidebar-toggle-btn toggle-remain btn btn-outline btn-primary btn-icon-right btn-rounded"
                  onClick={toggleSidebar}
                >
                  Filter
                  {type === "left" || type === "off-canvas" ? (
                    <i className="d-icon-arrow-left"></i>
                  ) : (
                    <i className="d-icon-arrow-right"></i>
                  )}
                </a>
                <ALink
                  href={{
                    pathname: router.pathname,
                    query: { grid: query.grid },
                  }}
                  scroll={false}
                  className="filter-clean"
                >
                  Clean All
                </ALink>
              </div>
            )}

            <div className="widget widget-collapsible">
              <Card
                title="<h3 class='widget-title'>Categorias<span class='toggle-btn p-0 parse-content'></span></h3>"
                type="parse"
                expanded={true}
              >
                <ul className="widget-body filter-items search-ul">
                  {renderDepartamentos()}
                </ul>
              </Card>
            </div>

            {isLogged && (
              <div className="widget widget-collapsible">
                <Card
                  title="<h3 class='widget-title'>Filtrar pelo preço<span class='toggle-btn p-0 parse-content'></span></h3>"
                  type="parse"
                  expanded={true}
                >
                  <div className="widget-body">
                    <form action="#">
                      <div className="filter-price-slider noUi-target noUi-ltr noUi-horizontal shop-input-range">
                        <InputRange
                          formatLabel={(value) => `${value}`}
                          maxValue={200}
                          minValue={0}
                          step={5}
                          value={filterPrice}
                          onChange={onChangePrice}
                        />
                      </div>

                      <div className="filter-actions">
                        <div className="filter-price-text mb-4">
                          Valor: R$ {filterPrice.min},00 - R$ {filterPrice.max}
                          ,00
                          <span className="filter-price-range"></span>
                        </div>

                        <button
                          className="btn btn-primary btn-filter btn-rounded"
                          onClick={filterByPrice}
                        >
                          Filtrar
                        </button>
                      </div>
                    </form>
                  </div>
                </Card>
              </div>
            )}

            {!user?.pessoa?.fornecedor && (
              <div className="widget widget-collapsible">
                <Card
                  title="<h3 class='widget-title'>Fornecedores<span class='toggle-btn p-0 parse-content'></span></h3>"
                  type="parse"
                  expanded={true}
                >
                  <input
                    type="text"
                    className="form-control"
                    name="search"
                    autoComplete="off"
                    value={fornecedoresSearch}
                    onChange={({ target: { value } }) => {
                      startLoadingFornecedores();
                      setFornecedoresSearch(value);
                      debounceFornecedores(value);
                    }}
                    placeholder="Filtre o fornecedor..."
                    required
                  />
                  <Collapse in={fornecedoresSelected.length > 0}>
                    <div className="row mt-1">
                      {fornecedoresSelected.map((item, index) => (
                        <div className="col mt-1">
                          <ALink
                            href="javascript: void(0);"
                            className="product-variation-clean"
                            onClick={() =>
                              handleRemoveSelectedFornecedor(index)
                            }
                          >
                            {item.fantasia.toUpperCase()}
                            <i className="d-icon-close ml-2"></i>
                          </ALink>
                        </div>
                      ))}
                    </div>
                  </Collapse>
                  <div
                    className="mt-3"
                    id="fornecedores-container"
                    style={{
                      minHeight: 150,
                    }}
                  >
                    {fornecedores.map((item) => (
                      <div className="form-checkbox mb-3" key={String(item.id)}>
                        <input
                          type="checkbox"
                          className="custom-checkbox"
                          id={item.id}
                          name={item.id}
                          onChange={(e) =>
                            handleSelectFornecedor(e?.target.checked, item)
                          }
                          checked={
                            !!fornecedoresSelected.find(
                              (itemSelected) => itemSelected.id === item.id
                            )
                          }
                        />
                        <label
                          className="form-control-label text-uppercase"
                          htmlFor={item.id}
                        >
                          {item.fantasia}
                        </label>
                      </div>
                    ))}
                  </div>
                </Card>
              </div>
            )}

            {(user?.pessoa?.cliente || !isLogged) && (
              <div className="widget widget-collapsible">
                <Card
                  title="<h3 class='widget-title'>Representantes<span class='toggle-btn p-0 parse-content'></span></h3>"
                  type="parse"
                  expanded={true}
                >
                  <input
                    type="text"
                    className="form-control"
                    name="search"
                    autoComplete="off"
                    value={representantesSearch}
                    onChange={({ target: { value } }) => {
                      startLoadingRepresentantes();
                      setRepresentantesSearch(value);
                      debounceRepresentantes(value);
                    }}
                    placeholder="Filtre o representante..."
                    required
                  />
                  <Collapse in={representantesSelected.length > 0}>
                    <div className="row mt-1">
                      {representantesSelected.map((item, index) => (
                        <div className="col mt-1">
                          <ALink
                            href="javascript: void(0);"
                            className="product-variation-clean"
                            onClick={() =>
                              handleRemoveSelectedRepresentante(index)
                            }
                          >
                            {item.fantasia.toUpperCase()}
                            <i className="d-icon-close ml-2"></i>
                          </ALink>
                        </div>
                      ))}
                    </div>
                  </Collapse>
                  <div
                    className="mt-3"
                    id="representantes-container"
                    style={{
                      minHeight: 150,
                    }}
                  >
                    {representantes.map((item) => (
                      <div className="form-checkbox mb-3" key={String(item.id)}>
                        <input
                          type="checkbox"
                          className="custom-checkbox"
                          id={item.id}
                          name={item.id}
                          onChange={(e) =>
                            handleSelectRepresentante(e?.target.checked, item)
                          }
                          checked={
                            !!representantesSelected.find(
                              (itemSelected) => itemSelected.id === item.id
                            )
                          }
                        />
                        <label
                          className="form-control-label text-uppercase"
                          htmlFor={item.id}
                        >
                          {item.fantasia}
                        </label>
                      </div>
                    ))}
                  </div>
                </Card>
              </div>
            )}

            {/* {isFeatured ? (
              <div className="widget widget-products widget-collapsible">
                <h4 className="widget-title">Our Featured</h4>

                <div className="widget-body">
                  <OwlCarousel adClass="owl-nav-top">
                    <div className="products-col">
                      {sidebarData.featured.slice(0, 3).map((item, index) => (
                        <SmallProduct
                          product={item}
                          key={item.name + " - " + index}
                        />
                      ))}
                    </div>
                    <div className="products-col">
                      {sidebarData.featured.slice(3, 6).map((item, index) => (
                        <SmallProduct
                          product={item}
                          key={item.name + " - " + index}
                        />
                      ))}
                    </div>
                  </OwlCarousel>
                </div>
              </div>
            ) : (
              ""
            )} */}
          </div>
        ) : (
          <div className="widget-2 mt-10 pt-5"></div>
        )}
      </div>
    </aside>
  );
}

export default SidebarFilterOne;
