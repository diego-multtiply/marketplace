import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { useRouter } from "next/router";
import Collapse from "react-bootstrap/Collapse";
import Image from "next/image";
import ALink from "@/components/features/custom-link";
import Countdown from "@/components/features/countdown";
import Quantity from "@/components/features/quantity";

import ProductNav from "@/components/partials/product/product-nav";

import { toggleWishlist } from "@/store/wishlist";

import { toDecimal } from "@/utils";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import useShop from "@/hooks/useShop";

function DetailOne(props) {
  let router = useRouter();
  const { data, isStickyCart = false, adClass = "", isNav = true } = props;
  const [curColor, setCurColor] = useState("null");
  const [curSize, setCurSize] = useState("null");
  const [curIndex, setCurIndex] = useState(-1);
  const [cartActive, setCartActive] = useState(false);
  const [quantity, setQauntity] = useState(1);

  const { data: wishlist = [] } = useAppSelector((state) => state.wishlist);
  const { user, isLogged } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  const { addToCart } = useShop();

  let product = data;

  // decide if the product is wishlisted
  let colors = [],
    sizes = [];

  const isWishlisted = useMemo(() => {
    return wishlist.findIndex((item) => item.slug === product?.slug) > -1
      ? true
      : false;
  }, [wishlist, product]);

  if (product && product?.variants?.length > 0) {
    if (product?.variants[0].size)
      product?.variants?.forEach((item) => {
        if (sizes.findIndex((size) => size.name === item.size.name) === -1) {
          sizes.push({ name: item.size.name, value: item.size.size });
        }
      });

    if (product?.variants[0].color) {
      product?.variants?.forEach((item) => {
        if (colors.findIndex((color) => color.name === item.color.name) === -1)
          colors.push({ name: item.color.name, value: item.color.color });
      });
    }
  }

  useEffect(() => {
    return () => {
      setCurIndex(-1);
      resetValueHandler();
    };
  }, [product]);

  useEffect(() => {
    if (product?.variants.length > 0) {
      if (
        (curSize !== "null" && curColor !== "null") ||
        (curSize === "null" &&
          product?.variants[0].size === null &&
          curColor !== "null") ||
        (curColor === "null" &&
          product?.variants[0].color === null &&
          curSize !== "null")
      ) {
        setCartActive(true);
        setCurIndex(
          product?.variants.findIndex(
            (item) =>
              (item.size !== null &&
                item.color !== null &&
                item.color.name === curColor &&
                item.size.name === curSize) ||
              (item.size === null && item.color.name === curColor) ||
              (item.color === null && item.size.name === curSize)
          )
        );
      } else {
        setCartActive(false);
      }
    } else {
      setCartActive(true);
    }

    if (product?.stock === 0) {
      setCartActive(false);
    }
  }, [curColor, curSize, product]);

  const wishlistHandler = (e) => {
    e.preventDefault();

    if (!isWishlisted) {
      let currentTarget = e.currentTarget;
      currentTarget.classList.add("load-more-overlay", "loading");
      dispatch(toggleWishlist(product));

      setTimeout(() => {
        currentTarget.classList.remove("load-more-overlay", "loading");
      }, 1000);
    } else {
      router.push("/pages/wishlist");
    }
  };

  const setColorHandler = (e) => {
    setCurColor(e.target.value);
  };

  const setSizeHandler = (e) => {
    setCurSize(e.target.value);
  };

  const addToCartHandler = async (e) => {
    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    let p;

    if (cartActive) {
      if (product?.variants.length > 0) {
        let tmpName = product?.name;
        let tmpPrice;
        let tempId;

        if (curColor !== "null" && curSize !== "null") {
          tmpName = product.variacoes.find(
            (item) =>
              item.cor === curColor &&
              (item.tamanho === curSize || item.numeracao === curSize)
          ).nome;
          tempId = product.variacoes.find(
            (item) =>
              item.cor === curColor &&
              (item.tamanho === curSize || item.numeracao === curSize)
          ).id;
        } else if (curColor !== "null") {
          tmpName = product.variacoes.find(
            (item) => item.cor === curColor
          ).nome;
          tempId = product.variacoes.find((item) => item.cor === curColor).id;
        } else if (curSize !== "null") {
          tmpName = product.variacoes.find(
            (item) => item.tamanho === curSize || item.numeracao === curSize
          ).nome;
          tempId = product.variacoes.find(
            (item) => item.tamanho === curSize || item.numeracao === curSize
          ).id;
        }

        if (product?.price[0] === product?.price[1]) {
          tmpPrice = product?.price[0];
        } else if (!product?.variants[0].price && product?.discount > 0) {
          tmpPrice = product?.price[0];
        } else {
          tmpPrice = product?.variants[curIndex].sale_price
            ? product?.variants[curIndex].sale_price
            : product?.variants[curIndex].price;
        }

        p = {
          ...product,
          id: tempId,
          idprodutoBase: product.id,
          name: tmpName,
          qty: quantity,
          price: tmpPrice,
        };
      } else {
        p = {
          ...product,
          idprodutoBase: null,
          qty: quantity,
          price: product?.price[0],
        };
      }

      try {
        await addToCart(p, quantity);
      } finally {
        currentTarget.classList.remove("load-more-overlay", "loading");
      }
    }
  };

  const resetValueHandler = () => {
    setCurColor("null");
    setCurSize("null");
  };

  function isDisabled(color, size) {
    if (color === "null" || size === "null") return false;

    if (sizes.length === 0) {
      return (
        product?.variants.findIndex((item) => item.color.name === curColor) ===
        -1
      );
    }

    if (colors.length === 0) {
      return (
        product?.variants.findIndex((item) => item.size.name === curSize) === -1
      );
    }

    return (
      product?.variants.findIndex(
        (item) => item.color.name === color && item.size.name === size
      ) === -1
    );
  }

  function changeQty(qty) {
    setQauntity(qty);
  }

  function handleNavigateToLogin() {
    router.push("/pages/login");
  }

  return (
    <div className={"product-details " + adClass}>
      {isNav && (
        <div className="product-navigation">
          <ul className="breadcrumb breadcrumb-lg">
            <li>
              <ALink href="/">
                <i className="d-icon-home"></i>
              </ALink>
            </li>
            <li>
              <ALink href="/shop" className="active">
                Produtos
              </ALink>
            </li>
            <li>Detalhes</li>
          </ul>

          <ProductNav product={product} />
        </div>
      )}

      <h2 className="product-name">{product?.name}</h2>

      <div className="product-meta">
        SKU: <span className="product-sku">{product?.sku}</span>
        <br />
        CATEGORIAS:{" "}
        <span className="product-brand">
          {product?.categories.map((item, index) => (
            <React.Fragment key={item.name + "-" + index}>
              <ALink
                href={{ pathname: "/shop", query: { category: item.slug } }}
              >
                {item.name}
              </ALink>
              {index < product?.categories.length - 1 ? ", " : ""}
            </React.Fragment>
          ))}
        </span>
        <br />
        FORNECEDOR:{" "}
        <span className="product-brand text-uppercase">
          <ALink
            href={{
              pathname: "shop",
              query: { category: product.idfornecedor },
            }}
          >
            {product.fornecedor}
          </ALink>
        </span>
        <br />
        QUANTIDADE:
        <span className="product-brand">{product?.quantity}</span>
      </div>

      {!isLogged ? null : user?.pessoa.cliente ? (
        <div className="product-price mb-2">
          <ins className="new-price">R$ {toDecimal(product.price)}</ins>
          {/* {product.price[1] && (
          <del className="old-price">R$ {toDecimal(product.price[1])}</del>
        )} */}
        </div>
      ) : (
        <div className="product-price mb-2">
          <ins className="new-price">R$ {toDecimal(product.minPrice)}</ins>
          {product.maxPrice && (
            <del className="new-price"> - R$ {toDecimal(product.maxPrice)}</del>
          )}
        </div>
      )}

      {product?.is_sale && <Countdown type={2} date="2024-10-10" />}

      {product?.is_review && (
        <div className="ratings-container">
          <div className="ratings-full">
            <span
              className="ratings"
              style={{ width: 20 * product?.ratings + "%" }}
            ></span>
            <span className="tooltiptext tooltip-top">
              {toDecimal(product?.ratings)}
            </span>
          </div>

          {
            <ALink href="#" className="rating-reviews">
              ( {product?.reviews} reviews )
            </ALink>
          }
        </div>
      )}

      {product?.short_description && (
        <p className="product-short-desc">{product?.short_description}</p>
      )}

      {product && product?.variants.length > 0 ? (
        <>
          {product?.variants[0].color && (
            <div className="product-form product-variations product-color">
              <label className="mr-5">Cor:</label>
              <div className="select-box">
                <select
                  name="color"
                  className="form-control select-color"
                  onChange={setColorHandler}
                  value={curColor}
                >
                  <option value="null">Escolha uma opção</option>
                  {colors.map((item) =>
                    !isDisabled(item.name, curSize) ? (
                      <option value={item.name} key={"color-" + item.name}>
                        {item.name}
                      </option>
                    ) : (
                      ""
                    )
                  )}
                </select>
              </div>
            </div>
          )}

          {product?.variants[0].size && (
            <div className="product-form product-variations product-size mb-0 pb-2">
              <label className="mr-4">Tamanho:</label>
              <div className="product-form-group">
                <div className="select-box">
                  <select
                    name="size"
                    className="form-control select-size"
                    onChange={setSizeHandler}
                    value={curSize}
                  >
                    <option value="null">Escolha uma opção</option>
                    {sizes.map((item) =>
                      !isDisabled(curColor, item.name) ? (
                        <option value={item.name} key={"size-" + item.name}>
                          {item.name}
                        </option>
                      ) : (
                        ""
                      )
                    )}
                  </select>
                </div>

                <Collapse in={"null" !== curColor || "null" !== curSize}>
                  <div className="card-wrapper overflow-hidden reset-value-button w-100 mb-0">
                    <ALink
                      href="javascript: void(0);"
                      className="product-variation-clean"
                      onClick={resetValueHandler}
                    >
                      Limpar
                    </ALink>
                  </div>
                </Collapse>
              </div>
            </div>
          )}

          {/* <div className="product-variation-price">
            <Collapse in={cartActive && curIndex > -1}>
              <div className="card-wrapper">
                {curIndex > -1 ? (
                  <div className="single-product-price">
                    {product?.variants[curIndex].price ? (
                      product?.variants[curIndex].sale_price ? (
                        <div className="product-price mb-0">
                          <ins className="new-price">
                            ${toDecimal(product?.variants[curIndex].sale_price)}
                          </ins>
                          <del className="old-price">
                            ${toDecimal(product?.variants[curIndex].price)}
                          </del>
                        </div>
                      ) : (
                        <div className="product-price mb-0">
                          <ins className="new-price">
                            ${toDecimal(product?.variants[curIndex].price)}
                          </ins>
                        </div>
                      )
                    ) : (
                      ""
                    )}
                  </div>
                ) : (
                  ""
                )}
              </div>
            </Collapse>
          </div> */}
        </>
      ) : (
        ""
      )}

      <hr className="product-divider"></hr>

      {isStickyCart ? (
        <div className="sticky-content fix-top product-sticky-content">
          <div className="container">
            <div className="sticky-product-details">
              <figure className="product-image">
                <ALink href={"/product/default/" + product?.slug}>
                  <Image
                    src={product?.pictures[0].url}
                    width="90"
                    height="90"
                    alt="Product"
                  />
                </ALink>
              </figure>
              <div>
                <h4 className="product-title">
                  <ALink href={"/product/default/" + product?.slug}>
                    {product?.name}
                  </ALink>
                </h4>
                <div className="product-info">
                  <div className="product-price mb-0">
                    {/* {curIndex > -1 && product?.variants[0] ? (
                      // product?.variants[curIndex].price ? (
                      //   product?.variants[curIndex].sale_price ? (
                      //     <>
                      //       <ins className="new-price">
                      //         $
                      //         {toDecimal(
                      //           product?.variants[curIndex].sale_price
                      //         )}
                      //       </ins>
                      //       <del className="old-price">
                      //         ${toDecimal(product?.variants[curIndex].price)}
                      //       </del>
                      //     </>
                      //   ) : (
                      //     <>
                      //       <ins className="new-price">
                      //         ${toDecimal(product?.variants[curIndex].price)}
                      //       </ins>
                      //     </>
                      //   )
                      // ) : (
                      //   ""
                      // )
                      ""
                    ) : (
                      <div className="product-price mb-2">
                        <ins className="new-price">
                          R$ {toDecimal(product.price[0])}
                        </ins>
                        {product.price[1] && (
                          <del className="old-price">
                            R$ {toDecimal(product.price[1])}
                          </del>
                        )}
                      </div>
                    )} */}
                    {isLogged && (
                      <div className="product-price mb-2">
                        <ins className="new-price">
                          R$ {toDecimal(product.price[0])}
                        </ins>
                        {product.price[1] && (
                          <del className="old-price">
                            R$ {toDecimal(product.price[1])}
                          </del>
                        )}
                      </div>
                    )}
                  </div>

                  {product?.is_review && (
                    <div className="ratings-container mb-0">
                      <div className="ratings-full">
                        <span
                          className="ratings"
                          style={{ width: 20 * product?.ratings + "%" }}
                        ></span>
                        <span className="tooltiptext tooltip-top">
                          {toDecimal(product?.ratings)}
                        </span>
                      </div>

                      <ALink href="#" className="rating-reviews">
                        ( {product?.reviews} reviews )
                      </ALink>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="product-form product-qty pb-0">
              <label className="d-none">QTY:</label>
              <div className="product-form-group align-items-end">
                <div>
                  {isLogged && (
                    <>
                      <div className="product-meta mb-0">
                        TOTAL DE ITENS:{" "}
                        <span className="product-sku">
                          {quantity * product?.quantity}
                        </span>
                      </div>
                      <Quantity
                        max={product?.stock}
                        product={product}
                        onChangeQty={changeQty}
                      />
                    </>
                  )}
                </div>

                {isLogged ? (
                  <button
                    className={`btn-product btn-cart text-normal ls-normal font-weight-semi-bold ${
                      cartActive ? "" : "disabled"
                    }`}
                    onClick={addToCartHandler}
                  >
                    <i className="d-icon-bag"></i>Adicionar ao Carrinho
                  </button>
                ) : (
                  <button
                    className={`btn-product btn-cart text-normal ls-normal font-weight-semi-bold`}
                    onClick={handleNavigateToLogin}
                    style={{
                      maxWidth: "25rem",
                    }}
                  >
                    FAÇA LOGIN PARA COMPRAR
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <>
          <div className="product-form product-qty pb-0">
            <label className="d-none">QTY:</label>
            {isLogged ? (
              <>
                <div className="product-meta">
                  TOTAL DE ITENS:{" "}
                  <span className="product-sku">
                    {quantity * product?.quantity}
                  </span>
                </div>
                <div className="product-form-group">
                  <Quantity
                    max={2000}
                    product={product}
                    onChangeQty={changeQty}
                  />
                  <button
                    className={`btn-product btn-cart text-normal ls-normal font-weight-semi-bold ${
                      cartActive ? "" : "disabled"
                    }`}
                    onClick={addToCartHandler}
                  >
                    <i className="d-icon-bag"></i>Adicionar ao Carrinho
                  </button>
                </div>
              </>
            ) : (
              <button
                className={`btn-product btn-cart text-normal ls-normal font-weight-semi-bold mb-2`}
                onClick={handleNavigateToLogin}
                style={{
                  maxWidth: "25rem",
                }}
              >
                FAÇA LOGIN PARA COMPRAR
              </button>
            )}
          </div>
        </>
      )}

      <hr className="product-divider mb-3"></hr>

      <div className="product-footer">
        {/* <div className="social-links mr-4">
          <ALink
            href="#"
            className="social-link social-facebook fab fa-facebook-f"
          ></ALink>
          <ALink
            href="#"
            className="social-link social-twitter fab fa-twitter"
          ></ALink>
          <ALink
            href="#"
            className="social-link social-pinterest fab fa-pinterest-p"
          ></ALink>
        </div>
        {" "} */}
        {/* <span className="divider d-lg-show"></span>{" "} */}
        {/* <a
          href="javascript: void(0);"
          className={`btn-product btn-wishlist`}
          title={
            isWishlisted
              ? "Navegue pelos seus favoritos"
              : "Adicionar aos Favoritos"
          }
          onClick={wishlistHandler}
        >
          <i
            className={isWishlisted ? "d-icon-heart-full" : "d-icon-heart"}
          ></i>{" "}
          {isWishlisted
            ? "Navegue pelos seus favoritos"
            : "Adicionar aos Favoritos"}
        </a> */}
      </div>
    </div>
  );
}

export default DetailOne;
