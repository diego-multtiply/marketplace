import React, { useEffect, useState } from "react";
import Reveal from "react-awesome-reveal";

import ALink from "@/components/features/custom-link";
import OwlCarousel from "@/components/features/owl-carousel";

import { productSlider2 } from "@/utils/data/carousel";
import { fadeIn } from "@/utils/data/keyframes";
import { useAppSelector } from "@/hooks/useStore";
import { getAllProducts } from "@/server/services/products";
import { useRouter } from "next/router";
import ProductFour from "@/components/features/product/product-four";
import ProductFive from "@/components/features/product/product-five";

function HomeCollection() {
  const [cat, setCat] = useState(null);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const { departamentos } = useAppSelector((state) => state.shop);

  const { isReady } = useRouter();

  useEffect(() => {
    async function loadProducts() {
      setLoading(true);

      const { data } = await getAllProducts(1, 15, {
        session: "H",
        category: cat ? cat : null,
      });

      if (data.success) {
        setProducts(
          data.data.data.map((dataItem) => ({
            ...dataItem,
            id: dataItem.id,
            slug: dataItem.slug,
            name: dataItem.nome,
            price: dataItem.preco,
            minPrice: Number(dataItem.preco_minimo),
            maxPrice: Number(dataItem.preco_maximo),
            discount: 0,
            short_description: dataItem.descricao,
            complementar_description: dataItem.descricao_complementar,
            collection: dataItem.descricao_colecao,
            sku: dataItem.sku,
            stock: 80,
            ratings: 5,
            reviews: 1,
            sale_count: 30,
            is_featured: true,
            is_new: false,
            is_sale: false,
            is_review: false,
            is_top: false,
            until: null,
            dimensions: [
              {
                name: "Altura",
                value: dataItem.altura,
              },
              {
                name: "Largura",
                value: dataItem.largura,
              },
              {
                name: "Comprimento",
                value: dataItem?.comprimento,
              },
            ],
            pictures: dataItem.imagens.map((image) => ({
              url:
                "https://cyhnwguzha.cloudimg.io/" +
                image.url +
                "?width=300&height=338",
              width: 300,
              height: 338,
            })),
            small_pictures: null,
            large_pictures: dataItem.imagens.map((image) => ({
              url:
                "https://cyhnwguzha.cloudimg.io/" +
                image.url +
                "?width=800&height=900",
              width: 800,
              height: 900,
            })),
            categories: [
              {
                name: dataItem.descricao_departamento,
                slug: dataItem.slug_departamento,
              },
              {
                name: dataItem.descricao_grupo_principal,
                slug: dataItem.slug_grupo_principal,
              },
              {
                name: dataItem.descricao_grupo_secundario,
                slug: dataItem.slug_grupo_secundario,
              },
            ],
            tags: [],
            brands: [
              {
                name: dataItem.marca,
              },
            ],
            variants: dataItem.variacoes.map((item) => ({
              price: 0,
              sale_price: null,
              color: item.cor
                ? {
                    name: item.cor,
                    slug: item.cor,
                    color: "#fff",
                  }
                : null,
              size: item.tamanho
                ? {
                    name: item.tamanho,
                    slug: item.tamanho,
                    size: item.tamanho,
                  }
                : item.numeracao
                ? {
                    name: item.numeracao,
                    slug: item.numeracao,
                    size: item.numeracao,
                  }
                : null,
            })),
            quantity: dataItem.volumes,
            un: dataItem.unidade,
          }))
        );
      }

      setLoading(false);
    }

    loadProducts();
  }, [cat, isReady]);

  return (
    <Reveal keyframes={fadeIn} duration={1200} triggerOnce>
      <div className="product-wrapper row mb-10 pb-2">
        <div className="col-xl-5col col-lg-3 col-md-4 mb-4">
          <div className="category-wrapper bg-white d-flex flex-column align-items-start h-100">
            <h2 className="title text-left">Para a sua casa</h2>
            <ul className="pl-0 mt-0 flex-1">
              {departamentos
                ?.filter((item) => item.tipo_sessao === "H")
                .map((item) => (
                  <li key={item.slug}>
                    <ALink
                      href="#"
                      className={cat === item.slug ? "active" : ""}
                      onClick={(e) => {
                        e.preventDefault();
                        setCat(item.slug);
                      }}
                    >
                      {item.descricao}
                    </ALink>
                  </li>
                ))}
            </ul>
            {/* <ALink
              href={{
                pathname: "/shop",
                query: { category: "clothing-apparel" },
              }}
              className="btn btn-dark btn-link btn-underline text-left"
            >
              Browse All<i className="d-icon-arrow-right"></i>
            </ALink> */}
          </div>
        </div>
        <div className="col-xl-5col4 col-lg-9 col-md-8 mb-4">
          {loading ? (
            <OwlCarousel
              adClass="owl-theme owl-nav-inner h-100"
              options={productSlider2}
            >
              {[1, 2, 3, 4, 5, 6, 7, 8].map((item) => (
                <div
                  className="product-wrap h-100 mb-0"
                  key={"clothing-" + item}
                >
                  <div className="product-loading-overlay"></div>
                </div>
              ))}
            </OwlCarousel>
          ) : (
            <OwlCarousel
              adClass="owl-theme owl-nav-inner h-100"
              options={productSlider2}
            >
              {products &&
                products.map((item, index) => (
                  <div
                    className="product-wrap h-100 mb-0"
                    key={`clothing-${index}`}
                  >
                    {item.variants.length > 0 ? (
                      <ProductFour product={item} adClass="" />
                    ) : (
                      <ProductFive product={item} adClass="" small />
                    )}
                  </div>
                ))}
            </OwlCarousel>
          )}
        </div>
      </div>
    </Reveal>
  );
}

export default React.memo(HomeCollection);
