import React from "react";

import OwlCarousel from "@/components/features/owl-carousel";

import { serviceSlider } from "@/utils/data/carousel";

function ServiceBox(props) {
  return (
    <OwlCarousel
      adClass="owl-theme service-slider bg-white mt-2 mb-10"
      options={serviceSlider}
    >
      <div className="icon-box icon-box-side icon-box1">
        <i className="icon-box-icon d-icon-layer"></i>
        <div className="icon-box-content">
          <h4 className="icon-box-title text-capitalize lh-1 ls-s">
            Variedade de Produtos
          </h4>
          <p className="lh-1">Mais de 25000 produtos cadastrados</p>
        </div>
      </div>

      <div className="icon-box icon-box-side icon-box2">
        <i className="icon-box-icon d-icon-service"></i>
        <div className="icon-box-content">
          <h4 className="icon-box-title text-capitalize lh-1 ls-s">
            Suporte Horário Comercial
          </h4>
          <p className="lh-1">Estamos aqui para te ajudar</p>
        </div>
      </div>

      <div className="icon-box icon-box-side icon-box3">
        <i className="icon-box-icon d-icon-secure"></i>
        <div className="icon-box-content">
          <h4 className="icon-box-title text-capitalize lh-1 ls-s">
            100% Seguro
          </h4>
          <p className="lh-1">Venda com segurança</p>
        </div>
      </div>

      <div className="icon-box icon-box-side icon-box4">
        <i className="icon-box-icon d-icon-percent"></i>
        <div className="icon-box-content">
          <h4 className="icon-box-title text-capitalize lh-1 ls-s">
            Ofertas Especiais
          </h4>
          <p className="lh-1">Desocontos e Ofertas</p>
        </div>
      </div>
    </OwlCarousel>
  );
}

export default React.memo(ServiceBox);
