import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";

import ALink from "@/components/features/custom-link";
import { useAppSelector } from "@/hooks/useStore";

function CategorySection() {
  const { departamentos } = useAppSelector((state) => state.shop);

  return (
    <div className="row cols-lg-6 cols-sm-3 cols-2 pt-6 pb-2">
      {departamentos?.map((item) => (
        <div className="category-wrap mb-4">
          <div className="category category-default1 category-absolute">
            <ALink href={{ pathname: "/shop", query: { category: item.slug } }}>
              <figure>
                <LazyLoadImage
                  src={
                    "https://cyhnwguzha.cloudimg.io/" +
                    item.imagem +
                    "?width=213&height=213"
                  }
                  alt="Category"
                  width="213"
                  height="213"
                />
              </figure>
              <div className="category-content">
                <h4 className="category-name">{item.descricao}</h4>
              </div>
            </ALink>
          </div>
        </div>
      ))}
    </div>
  );
}

export default React.memo(CategorySection);
