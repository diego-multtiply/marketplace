import { useEffect, useLayoutEffect } from "react";
import { connect } from "react-redux";
import { ToastContainer } from "react-toastify";
import { useRouter } from "next/router";
import "react-toastify/dist/ReactToastify.min.css";
import "react-image-lightbox/style.css";
import "react-input-range/lib/css/index.css";

import ALink from "@/components/features/custom-link";

import Header from "@/components/common/header";
import Footer from "@/components/common/footer";
import StickyFooter from "@/components/common/sticky-footer";
import Quickview from "@/components/features/product/common/quickview-modal";
import VideoModal from "@/components/features/modals/video-modal";
import MobileMenu from "@/components/common/partials/mobile-menu";

import {
  showScrollTopHandler,
  scrollTopHandler,
  stickyHeaderHandler,
  stickyFooterHandler,
  formatObject,
} from "@/utils";
import PageLoading from "./common/PageLoading";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import { toggleAppLoading } from "@/store/app";
import { getAllCategories } from "@/server/services/categories";
import { setDepartamentos } from "@/store/shop";
import { closeQuickview } from "@/store/modal";
import { setAuthorizationToken } from "@/server/api";
import { getProvidersCart } from "@/server/services/shop";
import { setProviders, setTotalProducts } from "@/store/cart";

function Layout({ children }) {
  const router = useRouter();

  const { isLogged, token, user } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  useEffect(() => {
    setAuthorizationToken(token?.token);
  }, [token]);

  useLayoutEffect(() => {
    document.querySelector("body")?.classList.remove("loaded");
  }, [router.pathname]);

  useEffect(() => {
    window.addEventListener("scroll", showScrollTopHandler, true);
    window.addEventListener("scroll", stickyHeaderHandler, true);
    window.addEventListener("scroll", stickyFooterHandler, true);
    window.addEventListener("resize", stickyHeaderHandler, true);
    window.addEventListener("resize", stickyFooterHandler, true);

    return () => {
      window.removeEventListener("scroll", showScrollTopHandler, true);
      window.removeEventListener("scroll", stickyHeaderHandler, true);
      window.removeEventListener("scroll", stickyFooterHandler, true);
      window.removeEventListener("resize", stickyHeaderHandler, true);
      window.removeEventListener("resize", stickyFooterHandler, true);
    };
  }, []);

  useEffect(() => {
    dispatch(closeQuickview());

    let bodyClasses = [...document.querySelector("body")?.classList];
    for (let i = 0; i < bodyClasses.length; i++) {
      document.querySelector("body")?.classList.remove(bodyClasses[i]);
    }

    setTimeout(() => {
      document.querySelector("body")?.classList.add("loaded");
    }, 50);
  }, [router.pathname]);

  useEffect(() => {
    async function loadData() {
      try {
        const { data } = await getAllCategories();
        dispatch(setDepartamentos(data.data));
      } finally {
        dispatch(toggleAppLoading());
      }
    }

    if (router.isReady) {
      loadData();
    }
  }, [router.isReady]);

  useEffect(() => {
    async function loadData() {
      const { data: providersCartData } = await getProvidersCart();

      if (user?.pessoa.fornecedor && providersCartData?.data.length) {
        dispatch(
          setTotalProducts(
            formatObject(providersCartData.data[0])?.quantidade_produtos
          )
        );
      } else {
        dispatch(setProviders(providersCartData.data));
      }
    }

    if (isLogged && router.isReady) {
      loadData();
    }
  }, [isLogged, router.isReady]);

  // if (isAppLoading) {
  //   return <PageLoading />;
  // }

  return (
    <>
      <div className="page-wrapper">
        <Header />
        {children}
        <Footer />
        <StickyFooter />
      </div>
      <ALink
        id="scroll-top"
        href="#"
        title="Top"
        role="button"
        className="scroll-top"
        onClick={() => scrollTopHandler(false)}
      >
        <i className="d-icon-arrow-up"></i>
      </ALink>
      <MobileMenu />
      <ToastContainer
        autoClose={3000}
        duration={300}
        newestOnTo={true}
        className="toast-container"
        position="bottom-left"
        closeButton={false}
        hideProgressBar={true}
        newestOnTop={true}
      />
      <Quickview />
      <VideoModal />
    </>
  );
}

export default Layout;
