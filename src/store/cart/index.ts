import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";

declare interface IFornecedoresCart {
  id: number;
  fornecedor: string;
  documento: string;
  quantidade_produtos: number;
  quantidade: number;
  quantidade_total: number;
  valor: number;
}
export interface CartState {
  data: any[];
  fornecedores: IFornecedoresCart[];
  totalProducts: number;
}

const initialState: CartState = {
  data: [],
  fornecedores: [],
  totalProducts: 0,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action: PayloadAction<any>) => {
      let tmpProduct = { ...action.payload };

      const index = state.data.find(
        (item) => item.idfornecedor === tmpProduct.idfornecedor
      );

      if (index) {
      }

      if (
        state.data.findIndex((item) => item.name === action.payload.name) > -1
      ) {
        let tmpData = state.data.reduce((acc, cur) => {
          if (cur.name === tmpProduct.name) {
            acc.push({
              ...cur,
              qty: parseInt(cur.qty) + parseInt(tmpProduct.qty),
            });
          } else {
            acc.push(cur);
          }

          return acc;
        }, []);

        state.data = tmpData;
      } else {
        state.data = [...state.data, tmpProduct];
      }
    },
    removeFromCart: (state, action: PayloadAction<any>) => {
      let cart = state.data.reduce((cartAcc, product) => {
        if (product.name !== action.payload.name) {
          cartAcc.push(product);
        }
        return cartAcc;
      }, []);

      state.data = cart;
    },
    updateCart: (state, action: PayloadAction<any>) => {
      state.data = action.payload;
    },
    setProviders: (state, action: PayloadAction<IFornecedoresCart[]>) => {
      state.fornecedores = action.payload;
    },
    setTotalProducts: (state, action: PayloadAction<number>) => {
      state.totalProducts = action.payload;
    },
  },
});

export const {
  addToCart,
  removeFromCart,
  updateCart,
  setProviders,
  setTotalProducts,
} = cartSlice.actions;

const cartPersistConfig = {
  keyPrefix: "new-app-",
  key: "cart",
  storage,
};

export default persistReducer(cartPersistConfig, cartSlice.reducer);
