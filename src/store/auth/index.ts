import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";
import { IUser } from "@/common/types/user";

export interface IToken {
  type: string;
  token: string;
  refreshToken: string;
  expires_at: string;
}

export interface AuthState {
  isLogged: boolean;
  user?: IUser;
  token?: IToken;
}

const initialState: AuthState = {
  isLogged: false,
  user: undefined,
  token: undefined,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<IUser>) => {
      state.user = action.payload;
    },
    setToken: (state, action: PayloadAction<IToken>) => {
      state.token = action.payload;
    },
    onLogin: (state) => {
      state.isLogged = true;
    },
    onLogout: (state) => {
      state.isLogged = false;
      state.user = undefined;
      state.token = undefined;
    },
  },
});

export const { setUser, setToken, onLogin, onLogout } = authSlice.actions;

const authPersistConfig = {
  keyPrefix: "multiweb-",
  key: "auth",
  storage,
};

export default persistReducer(authPersistConfig, authSlice.reducer);
