import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistStore, persistReducer } from "redux-persist";
import thunk from "redux-thunk";

import cart from "./cart/index";
import modal from "./modal/index";
import wishlist from "./wishlist/index";
import app from "./app/index";
import shop from "./shop/index";
import auth from "./auth/index";

const reducers = combineReducers({
  cart,
  modal,
  wishlist,
  app,
  shop,
  auth,
});

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["cart", "modal", "wishlist", "app"],
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk],
});

export const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
