import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";

// import { ChainStore, City, Client, Concept, State } from "../../types";

export interface ModalState {
  type: string;
  openModal: boolean;
  quickview: boolean;
  singleSlug: string;
}

const initialState: ModalState = {
  type: "video",
  openModal: false,
  quickview: false,
  singleSlug: "",
};

export const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    openModal: (state, action: PayloadAction<string>) => {
      state.singleSlug = action.payload;
      state.openModal = true;
    },
    closeModal: (state, action: PayloadAction<string>) => {
      state.openModal = false;
    },
    openQuickview: (state, action: PayloadAction<string>) => {
      state.quickview = true;
      state.singleSlug = action.payload;
    },
    closeQuickview: (state) => {
      state.quickview = false;
    },
  },
});

export const { openModal, closeModal, openQuickview, closeQuickview } =
  modalSlice.actions;

const modalPersistConfig = {
  keyPrefix: "new-app-",
  key: "modal",
  storage,
};

export default persistReducer(modalPersistConfig, modalSlice.reducer);
