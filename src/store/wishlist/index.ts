import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";

// import { ChainStore, City, Client, Concept, State } from "../../types";

export interface WishlistState {
  data: any[];
}

const initialState: WishlistState = {
  data: [],
};

export const wishlistSlice = createSlice({
  name: "wishlist",
  initialState,
  reducers: {
    toggleWishlist: (state, action: PayloadAction<any>) => {
      let index = state.data.findIndex(
        (item) => item.name === action.payload.name
      );
      let tmpData = [...state.data];

      if (index === -1) {
        tmpData.push(action.payload);
      } else {
        tmpData.splice(index);
      }

      state.data = tmpData;
    },
    removeFromWishlist: (state, action: PayloadAction<any>) => {
      let wishlist = state.data.reduce((wishlistAcc, product) => {
        if (product.name !== action.payload.name) {
          wishlistAcc.push(product);
        }
        return wishlistAcc;
      }, []);

      state.data = wishlist;
    },
    clear: (state, action: PayloadAction<any>) => {
      state.data = [];
    },
  },
});

export const { toggleWishlist, removeFromWishlist, clear } =
  wishlistSlice.actions;

const wishlistPersistConfig = {
  keyPrefix: "new-app-",
  key: "wishlist",
  storage,
};

export default persistReducer(wishlistPersistConfig, wishlistSlice.reducer);
