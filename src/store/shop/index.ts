import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";

export interface IGrupoSecundario {
  id: number;
  idcategoria_grupo_principal: number;
  codigo: number;
  descricao: string;
  slug: string;
  ativo: string;
}

export interface IGrupoPrincipal {
  id: number;
  idcategoria_departamento: number;
  codigo: number;
  descricao: string;
  slug: string;
  ativo: string;
  tipo_medida_tamanho: string;
  possui_colecao: string;
  grupoSecundario: IGrupoSecundario[];
}

export interface IDepartamento {
  id: number;
  codigo: number;
  descricao: string;
  slug: string;
  ativo: string;
  tipo_sessao: string;
  grupoPrincipal: IGrupoPrincipal[];
}

export interface ShopState {
  departamentos: IDepartamento[];
}

const initialState: ShopState = {
  departamentos: [],
};

export const shopSlice = createSlice({
  name: "shop",
  initialState,
  reducers: {
    setDepartamentos: (state, action: PayloadAction<IDepartamento[]>) => {
      state.departamentos = action.payload;
    },
  },
});

export const { setDepartamentos } = shopSlice.actions;

const shopPersistConfig = {
  keyPrefix: "multiweb-",
  key: "shop",
  storage,
};

export default persistReducer(shopPersistConfig, shopSlice.reducer);
