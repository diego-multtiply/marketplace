import { createSlice } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";

export interface AppState {
  isAppLoading: boolean;
}

const initialState: AppState = {
  isAppLoading: true,
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    toggleAppLoading: (state) => {
      state.isAppLoading = !state.isAppLoading;
    },
  },
});

export const { toggleAppLoading } = appSlice.actions;

const appPersistConfig = {
  keyPrefix: "multiweb-",
  key: "app",
  storage,
};

export default persistReducer(appPersistConfig, appSlice.reducer);
