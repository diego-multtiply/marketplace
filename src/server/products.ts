import data from "../../data.json";

export async function getProducts(slug: string) {
  return data.products
    .filter(
      (item) => item.categories.filter((cat) => cat.slug === slug).length > 0
    )
    .map((item) => {
      return {
        ...item,
        price: item.variants.map((v) => v.price),
      };
    });
}
