import { api } from "../api";

export const getAllProducts = async (
  page: string,
  perPage: string,
  query: {
    category: string;
    minPrice: string;
    maxPrice: string;
    provider: string;
    representative: string;
    session: string;
    search: string;
    sortby: string;
  }
) => {
  try {
    return await api.get("products", {
      params: {
        page,
        perPage,
        category: query.category,
        minPrice: query.minPrice,
        maxPrice: query.maxPrice,
        provider: query.provider,
        representative: query.representative,
        session: query.session,
        search: query.search,
        sortby: query.sortby,
      },
    });
  } catch (err) {
    return {
      data: [],
    };
  }
};

export const getProduct = async (slug: string) => {
  try {
    return await api.get(`products/${slug}`);
  } catch (err) {
    return {
      data: null,
    };
  }
};
