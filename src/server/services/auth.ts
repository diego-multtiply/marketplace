import { TipoPessoa } from "@/pages/pages/register";
import { api } from "../api";

export interface IRegisterUser {
  tipo: string;
  razaoSocial: string;
  fantasia: string;
  tipoPessoa: TipoPessoa.FISICA;
  cnpj: string;
  cpf: string;
  responsavel: string;
  wppComercial: string;
  emailComercial: string;
}

export interface ILoginUser {
  login: string;
  password: string;
}

export const registerUser = async (body: IRegisterUser) =>
  await api.post("auth/register", body);

export const loginUser = async (body: ILoginUser) =>
  await api.post("auth/login", body);
