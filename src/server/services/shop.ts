import { formatObject } from "@/utils";
import { api } from "../api";

export const addProductToCart = async (
  idfornecedor: number,
  idproduto: number,
  idprodutoBase: number | null,
  quantidade: number
) => {
  try {
    return await api.post("/shop/addToCart", {
      idfornecedor,
      idproduto,
      idprodutoBase,
      quantidade,
    });
  } catch (err) {
    return {
      data: [],
    };
  }
};

export const getProvidersCart = async () => {
  try {
    const response = await api.get("/shop/providers");

    response.data.data = response.data.data.map((item: any) => {
      return formatObject(item);
    });

    return response;
  } catch (err) {
    return {
      data: [],
    };
  }
};

export const getCart = async (
  idfornecedor: number,
  uf?: string = "",
  franchiseId?: number,
  clientId?: number
) => {
  try {
    const response = await api.get(`/shop/cart/${idfornecedor}`, {
      params: {
        uf,
        franchiseId,
        clientId,
      },
    });

    response.data.data.produtos = response.data.data.produtos.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    response.data.data.prazosPagamento = response.data.data.prazosPagamento.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    response.data.data.formasPagamento = response.data.data.formasPagamento.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    response.data.data.enderecos = response.data.data.enderecos.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    response.data.data.datasEntrega = response.data.data.datasEntrega.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    response.data.data.franquias = response.data.data.franquias.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    response.data.data.clientes = response.data.data.clientes.map(
      (item: any) => {
        return formatObject(item);
      }
    );

    return response;
  } catch (err) {
    return { data: {} };
  }
};

export const updateCart = async (cartProducts: any[], idfornecedor: number) => {
  try {
    return await api.put(`/shop/cart/${idfornecedor}`, {
      data: cartProducts,
    });
  } catch (err) {
    return {
      data: {},
    };
  }
};

export const removeFromCart = async (
  idproduto: number,
  idfornecedor: number
) => {
  try {
    return await api.post(`/shop/removeFromCart`, {
      idproduto,
      idfornecedor,
    });
  } catch (err) {
    return {
      data: {},
    };
  }
};

export const finishCart = async (cart: any, idfornecedor: number) => {
  try {
    return await api.post(`/shop/cart/${idfornecedor}/finish`, {
      ...cart,
    });
  } catch (err) {
    return {
      data: {},
    };
  }
};

export const getOrder = async (idpedido: number) => {
  try {
    return await api.get(`/shop/${idpedido}/order`);
  } catch (err) {
    return {
      data: {},
    };
  }
};

export const deleteCart = async (idfornecedor: number) => {
  try {
    return await api.delete(`/shop/${idfornecedor}`);
  } catch (err) {
    return {
      data: {},
    };
  }
};

export const getFilter = async (params) => {
  try {
    return await api.get(`/filter`, {
      params,
    });
  } catch (err) {
    return {
      data: {},
    };
  }
};
