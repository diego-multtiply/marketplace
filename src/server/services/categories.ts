import { api } from "../api";

export const getAllCategories = async () => {
  try {
    return await api.get("categories");
  } catch (err) {
    return {
      data: [],
    };
  }
};
