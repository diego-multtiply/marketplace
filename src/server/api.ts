import axios from "axios";

export const api = axios.create({
  // baseURL: "http://localhost:3333",
  baseURL: "http://132.226.166.24:3333",
});

export const setAuthorizationToken = (token?: string) => {
  if (token) {
    api.defaults.headers["Authorization"] = `Bearer ${token}`;
  } else {
    delete api.defaults.headers.Authorization;
  }
};

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status) {
      window.location.href = "/pages/logout";
    }

    return Promise.reject(error);
  }
);
