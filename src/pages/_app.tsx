import "@/styles/globals.css";

import type { AppProps } from "next/app";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Helmet from "react-helmet";

import Layout from "@/components/layout";
import { persistor, store } from "@/store";

import "public/sass/style.scss";
import { useEffect } from "react";

function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <PersistGate
        persistor={persistor}
        loading={
          <div className="loading-overlay">
            <div className="bounce-loader">
              <div className="bounce1"></div>
              <div className="bounce2"></div>
              <div className="bounce3"></div>
              <div className="bounce4"></div>
            </div>
          </div>
        }
      >
        <Helmet>
          <meta charSet="UTF-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />

          <title>Multiweb</title>

          <meta name="keywords" content="React Template" />
          <meta name="description" content="Multiweb" />
          <meta name="author" content="D-THEMES" />
        </Helmet>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </PersistGate>
    </Provider>
  );
}

export default App;
