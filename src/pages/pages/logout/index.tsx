import React, { useEffect } from "react";
import Helmet from "react-helmet";
import Image from "next/image";

import { parallaxHandler } from "@/utils";
import { useAppDispatch } from "@/hooks/useStore";
import { onLogout } from "@/store/auth";
import { useRouter } from "next/router";
import { setAuthorizationToken } from "@/server/api";

function Logout() {
  const dispatch = useAppDispatch();

  const { push } = useRouter();

  useEffect(() => {
    dispatch(onLogout());
    setAuthorizationToken();
    push("/");
  }, []);

  return (
    <main className="main">
      <Helmet>
        <title>Multiweb | Sair</title>
      </Helmet>

      <h1 className="d-none">Multiweb | Sair</h1>

      <div className="page-content">
        <section className="error-section d-flex flex-column justify-content-center align-items-center text-center pl-3 pr-3">
          <Image
            src="/images/subpages/404.png"
            alt="error 404"
            width="609"
            height="131"
          />
          <h4 className="mt-7 mb-0 ls-m text-uppercase">Saindo...</h4>
        </section>
      </div>
    </main>
  );
}

export default React.memo(Logout);
