import React, { useEffect } from "react";
import Helmet from "react-helmet";
import CountUp from "react-countup";
import Reveal from "react-awesome-reveal";
import { LazyLoadImage } from "react-lazy-load-image-component";

import ALink from "@/components/features/custom-link";
import OwlCarousel from "@/components/features/owl-carousel";

import { fadeIn, fadeInLeftShorter } from "@/utils/data/keyframes";
import { mainSlider16 } from "@/utils/data/carousel";

function AboutUs() {
  useEffect(() => {
    countToHandler();
    window.addEventListener("scroll", countToHandler, true);

    return () => {
      window.removeEventListener("scroll", countToHandler);
    };
  }, []);

  function countToHandler() {
    let items = document.querySelectorAll(".count-to");

    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      if (
        item.getBoundingClientRect().top > 0 &&
        window.innerHeight - item.offsetHeight >
          item.getBoundingClientRect().top &&
        !item.classList.contains("finished")
      ) {
        if (item.querySelector("button")) item.querySelector("button").click();
        item.classList.add("finished");
      }
    }
  }

  return (
    <main className="main about-us">
      <Helmet>
        <title>Multiweb | Sobre a Multiweb</title>
      </Helmet>

      <h1 className="d-none">Multiweb | Sobre a Multiweb</h1>

      <nav className="breadcrumb-nav">
        <div className="container">
          <ul className="breadcrumb">
            <li>
              <ALink href="/">
                <i className="d-icon-home"></i>
              </ALink>
            </li>
            <li>Sobre a Multiweb</li>
          </ul>
        </div>
      </nav>
      <div
        className="page-header pl-4 pr-4"
        style={{
          backgroundImage: `url( /images/estampa.png )`,
          backgroundColor: "#3C63A4",
          backgroundRepeat: "repeat",
          backgroundSize: "contain",
        }}
      >
        <h3 className="page-subtitle font-weight-bold">Bem Vindo a Multiweb</h3>
        <h1 className="page-title font-weight-bold lh-1 text-white text-capitalize">
          Quem Somos
        </h1>
      </div>

      <div className="page-content">
        <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <div
            className="page-header pl-4 pr-4"
            style={{
              backgroundColor: "#fff",
            }}
          >
            <h5
              className="page-desc mb-0"
              style={{
                color: "#222",
              }}
            >
              Bem-vindo à MultiWeb, uma empresa B2B que transforma a experiência
              de
              <br />
              compra online. Fundada pelo nosso CEO, Diego Calegari, dono da
              Multi10
              <br />
              Franchising e CESLA, a MultiWeb conecta representantes com
              lojistas,
              <br />
              facilitando negociações e otimizando o processo de compra e venda
              no varejo.
            </h5>
          </div>
        </Reveal>

        <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <section className="about-section pb-10">
            <div className="container">
              <div className="row align-items-center">
                {/* <div className="col-lg-4 mb-10 mb-lg-4">
                  <h5 className="section-subtitle lh-2 ls-md font-weight-normal">
                    01. What We Do
                  </h5>
                  <h3 className="section-title lh-1 font-weight-bold">
                    Provide perfect and practical services
                  </h3>
                  <p className="section-desc">
                    Lorem quis bibendum auctar, nisi elit consequat ipsum, nec
                    sagittis sem nibh id elit.
                  </p>
                </div> */}
                <div className="col-lg-12">
                  <div className="row">
                    <div className="col-md-4 mb-4">
                      <div className="counter text-center text-dark">
                        <CountUp start={0} end={1438} duration={4}>
                          {({ countUpRef, start }) => (
                            <div className="count-to">
                              <span ref={countUpRef} />
                              <button onClick={start} className="d-none">
                                Start
                              </button>
                            </div>
                          )}
                        </CountUp>
                        <h5 className="count-title font-weight-bold text-body ls-md">
                          Fornecedores
                        </h5>
                        <p className="text-grey mb-0">
                          Uma vasta rede de parceiros prontos para atender suas
                          necessidades.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4 mb-4">
                      <div className="counter text-center text-dark">
                        <CountUp start={0} end={53854} duration={4}>
                          {({ countUpRef, start }) => (
                            <div className="count-to">
                              <span ref={countUpRef} />
                              <button onClick={start} className="d-none">
                                Start
                              </button>
                            </div>
                          )}
                        </CountUp>
                        <h5 className="count-title font-weight-bold text-body ls-md">
                          Produtos
                        </h5>
                        <p className="text-grey mb-0">
                          Uma seleção diversificada e de qualidade para atender
                          todas as demandas.
                        </p>
                      </div>
                    </div>
                    <div className="col-md-4 mb-4">
                      <div className="counter text-center text-dark">
                        <CountUp start={0} end={371} duration={4}>
                          {({ countUpRef, start }) => (
                            <div className="count-to">
                              <span ref={countUpRef} />
                              <button onClick={start} className="d-none">
                                Start
                              </button>
                            </div>
                          )}
                        </CountUp>
                        <h5 className="count-title font-weight-bold text-body ls-md">
                          Clientes Satisfeitos
                        </h5>
                        <p className="text-grey mb-0">
                          Uma base crescente de clientes que confiam e prosperam
                          conosco.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Reveal>

        <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <section className="customer-section pb-10">
            <div className="container">
              <div className="row align-items-center">
                <div className="col-md-7 mb-4">
                  <figure>
                    <LazyLoadImage
                      src="/images/subpages/customer.jpg"
                      alt="Happy Customer"
                      width="580"
                      height="507"
                      effect="opacity"
                      className="banner-radius"
                      style={{ backgroundColor: "#BDD0DE" }}
                    />
                  </figure>
                </div>
                <div className="col-md-5 mb-4">
                  {/* <h5 className="section-subtitle lh-2 ls-md font-weight-normal">
                    01. Nosso Propósito
                  </h5> */}
                  <h3 className="section-title lh-1 font-weight-bold">
                    01. Nosso Propósito
                  </h3>
                  <p className="section-desc text-grey">
                    Na MultiWeb, nossa missão é transformar a experiência de
                    compra online, conectando pessoas a produtos e serviços de
                    forma rápida, fácil e confiável. Buscamos ser a plataforma
                    de e-commerce preferida, onde inovação e conveniência se
                    encontram para oferecer uma jornada de compra intuitiva e
                    satisfatória
                  </p>
                  {/* <ALink
                    href="#"
                    className="btn btn-dark btn-link btn-underline ls-m"
                  >
                    Visit Our Store<i className="d-icon-arrow-right"></i>
                  </ALink> */}
                </div>
              </div>
            </div>
          </section>
        </Reveal>

        <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <section className="store-section">
            <div className="container">
              <div className="row align-items-center">
                <div className="col-md-6 order-md-first mb-4">
                  {/* <h5 className="section-subtitle lh-2 ls-md font-weight-normal mb-1">
                    03. Our Store
                  </h5> */}
                  <h3 className="section-title lh-1 font-weight-bold">
                    02. Nossa Missão
                  </h3>
                  <p className="section-desc text-grey">
                    Conectar fornecedores, representantes e lojistas em um
                    ambiente digital eficiente e inovador. Facilitamos
                    transações de compra e venda no varejo através de uma
                    plataforma robusta e intuitiva que otimiza a experiência de
                    negócios e impulsiona o crescimento econômico.
                  </p>
                  {/* <ALink
                    href="#"
                    className="btn btn-dark btn-link btn-underline ls-m"
                  >
                    Get Our Store<i className="d-icon-arrow-right"></i>
                  </ALink> */}
                </div>

                <div className="col-md-6 mb-4">
                  <figure>
                    <LazyLoadImage
                      src="/images/subpages/store.jpg"
                      alt="Our Store"
                      width="580"
                      height="507"
                      effect="opacity"
                      className="banner-radius"
                      style={{ backgroundColor: "#DEE6E8" }}
                    />
                  </figure>
                </div>
              </div>
            </div>
          </section>
        </Reveal>

        <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <div
            className="page-header pl-4 pr-4"
            style={{
              backgroundColor: "#fff",
            }}
          >
            <h5
              className="page-desc mb-0"
              style={{
                color: "#222",
              }}
            >
              Na MultiWeb, acreditamos que cada compra é uma oportunidade de
              <br />
              inovar e simplificar. Junte-se a nós e descubra como podemos
              <br />
              transformar sua experiência de compra online, oferecendo sempre o
              <br />
              melhor em eficiência, economia e satisfação.
            </h5>
          </div>
        </Reveal>

        {/* <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <section className="brand-section grey-section pt-10 pb-10">
            <div className="container mt-8 mb-10">
              <h5 className="section-subtitle lh-2 ls-md font-weight-normal mb-1 text-center">
                04. Our Clients
              </h5>
              <h3 className="section-title lh-1 font-weight-bold text-center mb-5">
                Popular Brands
              </h3>

              <OwlCarousel adClass="owl-theme" options={mainSlider16}>
                <figure className="brand-wrap bg-white banner-radius">
                  <img
                    src="/images/brands/1.png"
                    alt="Brand"
                    width="180"
                    height="100"
                  />
                </figure>
                <figure className="brand-wrap bg-white banner-radius">
                  <img
                    src="/images/brands/2.png"
                    alt="Brand"
                    width="180"
                    height="100"
                  />
                </figure>
                <figure className="brand-wrap bg-white banner-radius">
                  <img
                    src="/images/brands/3.png"
                    alt="Brand"
                    width="180"
                    height="100"
                  />
                </figure>
                <figure className="brand-wrap bg-white banner-radius">
                  <img
                    src="/images/brands/4.png"
                    alt="Brand"
                    width="180"
                    height="100"
                  />
                </figure>
                <figure className="brand-wrap bg-white banner-radius">
                  <img
                    src="/images/brands/5.png"
                    alt="Brand"
                    width="180"
                    height="100"
                  />
                </figure>
                <figure className="brand-wrap bg-white banner-radius">
                  <img
                    src="/images/brands/6.png"
                    alt="Brand"
                    width="180"
                    height="100"
                  />
                </figure>
              </OwlCarousel>
            </div>
          </section>
        </Reveal>

        <Reveal keyframes={fadeIn} delay="50" duration="1000" triggerOnce>
          <section className="team-section pt-8 mt-10 pb-10 mb-6">
            <div className="container">
              <h5 className="section-subtitle lh-2 ls-md font-weight-normal mb-1 text-center">
                05. Our Leaders
              </h5>
              <h3 className="section-title lh-1 font-weight-bold text-center mb-5">
                Meet our team
              </h3>
              <div className="row cols-sm-2 cols-md-4">
                <Reveal
                  keyframes={fadeInLeftShorter}
                  delay="20"
                  duration="1000"
                  triggerOnce
                >
                  <div className="member">
                    <figure className="banner-radius">
                      <LazyLoadImage
                        src="/images/subpages/team1.jpg"
                        alt="Oteam member"
                        width={280}
                        height={280}
                        effect="opacity"
                        style={{ backgroundColor: "#EEE" }}
                      />

                      <div className="overlay social-links">
                        <ALink
                          href="#"
                          className="social-link social-facebook fab fa-facebook-f"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-twitter fab fa-twitter"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-linkedin fab fa-linkedin-in"
                        ></ALink>
                      </div>
                    </figure>

                    <h4 className="member-name">Tomasz Treflerzan</h4>
                    <h5 className="member-job">Ceo / Founder</h5>
                  </div>
                </Reveal>

                <Reveal
                  keyframes={fadeInLeftShorter}
                  delay="30"
                  duration="1000"
                  triggerOnce
                >
                  <div className="member">
                    <figure className="banner-radius">
                      <LazyLoadImage
                        src="/images/subpages/team2.jpg"
                        alt="Oteam member"
                        width={280}
                        height={280}
                        effect="opacity"
                        style={{ backgroundColor: "#EEE" }}
                      />

                      <div className="overlay social-links">
                        <ALink
                          href="#"
                          className="social-link social-facebook fab fa-facebook-f"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-twitter fab fa-twitter"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-linkedin fab fa-linkedin-in"
                        ></ALink>
                      </div>
                    </figure>

                    <h4 className="member-name">Dylan Chavez</h4>
                    <h5 className="member-job">Support manager / founder</h5>
                  </div>
                </Reveal>

                <Reveal
                  keyframes={fadeInLeftShorter}
                  delay="40"
                  duration="1000"
                  triggerOnce
                >
                  <div className="member">
                    <figure className="banner-radius">
                      <LazyLoadImage
                        src="/images/subpages/team3.jpg"
                        alt="Oteam member"
                        width={280}
                        height={280}
                        effect="opacity"
                        style={{ backgroundColor: "#EEE" }}
                      />

                      <div className="overlay social-links">
                        <ALink
                          href="#"
                          className="social-link social-facebook fab fa-facebook-f"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-twitter fab fa-twitter"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-linkedin fab fa-linkedin-in"
                        ></ALink>
                      </div>
                    </figure>

                    <h4 className="member-name">Viktoriia Demianenko</h4>
                    <h5 className="member-job">Designer</h5>
                  </div>
                </Reveal>

                <Reveal
                  keyframes={fadeInLeftShorter}
                  delay="50"
                  duration="1000"
                  triggerOnce
                >
                  <div className="member">
                    <figure className="banner-radius">
                      <LazyLoadImage
                        src="/images/subpages/team4.jpg"
                        alt="Oteam member"
                        width={280}
                        height={280}
                        effect="opacity"
                        style={{ backgroundColor: "#EEE" }}
                      />

                      <div className="overlay social-links">
                        <ALink
                          href="#"
                          className="social-link social-facebook fab fa-facebook-f"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-twitter fab fa-twitter"
                        ></ALink>
                        <ALink
                          href="#"
                          className="social-link social-linkedin fab fa-linkedin-in"
                        ></ALink>
                      </div>
                    </figure>

                    <h4 className="member-name">Mikhail Hnatuk</h4>
                    <h5 className="member-job">Support</h5>
                  </div>
                </Reveal>
              </div>
            </div>
          </section>
        </Reveal> */}
      </div>
    </main>
  );
}

export default React.memo(AboutUs);
