import React, { useState } from "react";
import Helmet from "react-helmet";

import ALink from "@/components/features/custom-link";
import { ILoginUser, loginUser, registerUser } from "@/server/services/auth";
import Alert from "@/components/common/alert";
import { useRouter } from "next/navigation";
import { useAppDispatch } from "@/hooks/useStore";
import { onLogin, setToken, setUser } from "@/store/auth";
import { toast } from "react-toastify";
import AlertPopup from "@/components/features/product/common/alert-popup";

function Login() {
  const [form, setForm] = useState<ILoginUser>({
    login: "",
    password: "",
  });

  const [error, setError] = useState({
    isVisible: false,
    message: "",
  });

  const dispatch = useAppDispatch();

  const { push } = useRouter();

  async function handleSubmit(e) {
    e.preventDefault();

    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setError({
      isVisible: false,
      message: "",
    });

    const body = {
      ...form,
    };

    try {
      const response = await loginUser(body);
      const data = response?.data;

      if (!data.success && data.message) {
        return setError({
          isVisible: true,
          message: data.message,
        });
      } else if (!data.success) {
        return toast(
          <AlertPopup
            message={
              "Atenção! Houve um erro, por favor, entre em contato com o suporte da plataforma para mais informações."
            }
          />,
          {
            position: "top-right",
          }
        );
      }

      dispatch(setUser(data.data.user));
      dispatch(setToken(data.data.token));
      dispatch(onLogin());

      push("/");
    } catch (err) {
      return toast(
        <AlertPopup
          message={
            "Atenção! Houve um erro, por favor, entre em contato com o suporte da plataforma para mais informações."
          }
        />,
        {
          position: "top-right",
        }
      );
    } finally {
      currentTarget.classList.remove("load-more-overlay", "loading");
    }
  }

  return (
    <main className="main register">
      <Helmet>
        <title>Multiweb - Login</title>
      </Helmet>

      <nav className="breadcrumb-nav">
        <div className="container">
          <ul className="breadcrumb">
            <li>
              <ALink ink href="/">
                <i className="d-icon-home"></i>
              </ALink>
            </li>
            <li>Login</li>
          </ul>
        </div>
      </nav>
      <div className="page-content mt-6 pb-2 mb-10">
        <div className="container">
          <div className="login-popup">
            <div className="form-box">
              <span className="nav-link border-no lh-1 ls-normal">Login</span>

              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <label htmlFor="user">Usuário</label>
                  <input
                    type="text"
                    className="form-control"
                    id="user"
                    name="user"
                    placeholder="Usuário *"
                    required
                    value={form.login}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        login: value,
                      })
                    }
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Senha</label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    name="password"
                    placeholder="Senha *"
                    required
                    value={form.password}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        password: value,
                      })
                    }
                  />
                </div>
                {error.isVisible && (
                  <Alert type="warning" text={error.message} className="mb-2" />
                )}
                <button
                  className="btn btn-dark btn-block btn-rounded"
                  type="submit"
                >
                  Entrar
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default React.memo(Login);
