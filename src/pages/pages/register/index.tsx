import React, { useMemo, useState } from "react";
import Helmet from "react-helmet";

import ALink from "@/components/features/custom-link";
import { IRegisterUser, registerUser } from "@/server/services/auth";
import { formatCnpj, formatCpf, formatPhone } from "@/utils/mask";
import Alert from "@/components/common/alert";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import AlertPopup from "@/components/features/product/common/alert-popup";

export enum TipoPessoa {
  FISICA = "F",
  JURIDICA = "J",
}

function Register() {
  const [form, setForm] = useState<IRegisterUser>({
    tipo: "C",
    razaoSocial: "",
    fantasia: "",
    tipoPessoa: TipoPessoa.FISICA,
    cnpj: "",
    cpf: "",
    responsavel: "",
    wppComercial: "",
    emailComercial: "",
  });

  const [error, setError] = useState({
    isVisible: false,
    message: "",
  });

  const { push } = useRouter();

  const nome = useMemo(() => {
    return form.tipoPessoa === TipoPessoa.FISICA ? "Nome" : "Razão Social";
  }, [form.tipoPessoa]);

  const apelido = useMemo(() => {
    return form.tipoPessoa === TipoPessoa.FISICA ? "Apelido" : "Nome Fantasia";
  }, [form.tipoPessoa]);

  async function handleSubmit(e) {
    e.preventDefault();

    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setError({
      isVisible: false,
      message: "",
    });

    const body = {
      ...form,
    };

    try {
      const response = await registerUser(body);
      const data = response?.data;

      if (!data.success && data.message) {
        return setError({
          isVisible: true,
          message: data.message,
        });
      } else if (!data.success) {
        return toast(
          <AlertPopup
            message={
              "Atenção! Houve um erro, por favor, entre em contato com o suporte da plataforma para mais informações."
            }
          />,
          {
            position: "top-right",
          }
        );
      }

      push("/pages/register/success");
    } catch (err) {
      return toast(
        <AlertPopup
          message={
            "Atenção! Houve um erro, por favor, entre em contato com o suporte da plataforma para mais informações."
          }
        />,
        {
          position: "top-right",
        }
      );
    } finally {
      currentTarget.classList.remove("load-more-overlay", "loading");
    }
  }

  return (
    <main className="main register">
      <Helmet>
        <title>Multiweb - Registrar</title>
      </Helmet>

      <nav className="breadcrumb-nav">
        <div className="container">
          <ul className="breadcrumb">
            <li>
              <ALink ink href="/">
                <i className="d-icon-home"></i>
              </ALink>
            </li>
            <li>Registrar</li>
          </ul>
        </div>
      </nav>
      <div className="page-content mt-6 pb-2 mb-10">
        <div className="container">
          <div className="login-popup">
            <div className="form-box">
              <span className="nav-link border-no lh-1 ls-normal">
                Registrar
              </span>

              <form onSubmit={handleSubmit}>
                <div className="d-flex w-100 align-items-center justify-content-center mb-3">
                  <div className="custom-radio">
                    <input
                      type="radio"
                      id="tipo_cliente"
                      name="tipo"
                      className="custom-control-input"
                      checked={form.tipo === "C"}
                      onChange={() =>
                        setForm({
                          ...form,
                          tipo: "C",
                        })
                      }
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="tipo_cliente"
                    >
                      Cliente
                    </label>
                  </div>
                  <div className="custom-radio ml-2">
                    <input
                      type="radio"
                      id="tipo_fornecedor"
                      name="tipo"
                      className="custom-control-input"
                      checked={form.tipo === "F"}
                      onChange={() =>
                        setForm({
                          ...form,
                          tipo: "F",
                        })
                      }
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="tipo_fornecedor"
                    >
                      Fornecedor
                    </label>
                  </div>
                  <div className="custom-radio ml-2">
                    <input
                      type="radio"
                      id="tipo_representante"
                      name="tipo"
                      className="custom-control-input"
                      checked={form.tipo === "R"}
                      onChange={() =>
                        setForm({
                          ...form,
                          tipo: "R",
                        })
                      }
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="tipo_representante"
                    >
                      Representante
                    </label>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="tipo_pessoa">Tipo de Pessoa</label>
                  <div className="select-box">
                    <select
                      id="tipo_pessoa"
                      name="tipo_pessoa"
                      className="form-control"
                      value={form.tipoPessoa}
                      onChange={({ target: { value } }) =>
                        setForm({
                          ...form,
                          tipoPessoa: value,
                        })
                      }
                    >
                      <option value="F">Física</option>
                      <option value="J">Jurídica</option>
                    </select>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="razao_social">{nome}</label>
                  <input
                    type="text"
                    className="form-control"
                    id="razao_social"
                    name="razao_social"
                    placeholder={`${nome} *`}
                    required
                    value={form.razaoSocial}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        razaoSocial: value,
                      })
                    }
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="fantasia">{apelido}</label>
                  <input
                    type="text"
                    className="form-control"
                    id="fantasia"
                    name="fantasia"
                    placeholder={`${apelido} *`}
                    required
                    value={form.fantasia}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        fantasia: value,
                      })
                    }
                  />
                </div>
                {form.tipoPessoa === TipoPessoa.FISICA ? (
                  <div className="form-group">
                    <label htmlFor="cpf">CPF</label>
                    <input
                      type="text"
                      className="form-control"
                      id="cpf"
                      name="cpf"
                      placeholder="CPF *"
                      required
                      value={form.cpf}
                      maxLength={14}
                      onChange={({ target: { value } }) =>
                        setForm({
                          ...form,
                          cpf: formatCpf(value),
                        })
                      }
                    />
                  </div>
                ) : (
                  <div className="form-group">
                    <label htmlFor="cnpj">CNPJ</label>
                    <input
                      type="text"
                      className="form-control"
                      id="cnpj"
                      name="cnpj"
                      placeholder="CNPJ *"
                      required
                      value={form.cnpj}
                      maxLength={18}
                      onChange={({ target: { value } }) =>
                        setForm({
                          ...form,
                          cnpj: formatCnpj(value),
                        })
                      }
                    />
                  </div>
                )}
                <div className="form-group">
                  <label htmlFor="responsavel">Responsável</label>
                  <input
                    type="text"
                    className="form-control"
                    id="responsavel"
                    name="responsavel"
                    placeholder="Responsável *"
                    required
                    value={form.responsavel}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        responsavel: value,
                      })
                    }
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="wpp_comercial">WhatsApp Comercial</label>
                  <input
                    type="text"
                    className="form-control"
                    id="wpp_comercial"
                    name="wpp_comercial"
                    placeholder="WhatsApp Comercial *"
                    required
                    value={form.wppComercial}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        wppComercial: formatPhone(value),
                      })
                    }
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email_comercial">E-mail Comercial</label>
                  <input
                    type="email"
                    className="form-control"
                    id="email_comercial"
                    name="email_comercial"
                    placeholder="E-mail Comercial *"
                    required
                    value={form.emailComercial}
                    onChange={({ target: { value } }) =>
                      setForm({
                        ...form,
                        emailComercial: value,
                      })
                    }
                  />
                </div>
                {error.isVisible && (
                  <Alert type="warning" text={error.message} className="mb-2" />
                )}
                <button
                  className="btn btn-dark btn-block btn-rounded"
                  type="submit"
                >
                  Registrar
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default React.memo(Register);
