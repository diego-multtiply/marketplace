import ALink from "@/components/features/custom-link";

import { setProviders, setTotalProducts } from "@/store/cart";

import { formatObject, toDecimal } from "@/utils";
import { useAppDispatch, useAppSelector } from "@/hooks/useStore";
import { deleteCart, getProvidersCart } from "@/server/services/shop";

function Cart() {
  const { fornecedores } = useAppSelector((state) => state.cart);
  const { isLogged, user } = useAppSelector((state) => state.auth);

  const dispatch = useAppDispatch();

  async function handleDeleteCart(e, idfornecedor: number) {
    e.preventDefault();

    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    try {
      await deleteCart(idfornecedor);

      if (isLogged) {
        const { data: providersCartData } = await getProvidersCart();

        if (user?.pessoa.fornecedor) {
          dispatch(
            setTotalProducts(
              formatObject(providersCartData.data[0])?.quantidade_produtos
            )
          );
        } else {
          dispatch(setProviders(providersCartData.data));
        }
      }
    } finally {
      currentTarget.classList.remove("load-more-overlay", "loading");
    }
  }

  return (
    <div className="main cart border-no">
      <div className="page-content pt-7 pb-10">
        {!fornecedores?.length ? (
          <div className="empty-cart text-center">
            <p>Seu carrinho está vazio.</p>
            <i className="cart-empty d-icon-bag"></i>
            <p className="return-to-shop mb-0">
              <ALink
                className="button wc-backward btn btn-primary"
                href="/shop"
              >
                Voltar para a Loja
              </ALink>
            </p>
          </div>
        ) : (
          <>
            <div className="container-fluid">
              <div className="banner cta-simple">
                {fornecedores.map((fornecedor, index) => (
                  <div
                    className="banner-content bg-white d-lg-flex align-items-center pt-3 pb-3"
                    key={fornecedor.fornecedor}
                  >
                    <div className="banner-header pr-lg-7 pb-lg-0 pb-4 mb-lg-0 mb-6">
                      <h4 className="banner-title font-weight-bold ls-s text-uppercase">
                        {user?.pessoa.fornecedor
                          ? user?.nome
                          : fornecedor.fornecedor}
                      </h4>
                      <h5 className="banner-subtitle font-weight-normal ls-s text-body">
                        {user?.pessoa.fornecedor
                          ? user?.pessoa.cnpj || user?.pessoa.cpf
                          : fornecedor.documento}
                      </h5>
                    </div>
                    <div className="banner-text mb-lg-0 mb-4 mr-lg-4 pl-lg-6 pr-lg-0 pl-2 pr-2">
                      <h6 className="font-weight-normal ls-normal text-uppercase mb-0">
                        Quantidade de Produtos: {fornecedor.quantidade_produtos}
                      </h6>
                      <h6 className="font-weight-normal ls-normal text-uppercase mb-0">
                        Quantidade Total: {fornecedor.quantidade_total}
                      </h6>
                      {user?.pessoa.cliente && (
                        <h6 className="font-weight-normal ls-normal text-uppercase mb-0">
                          Valor Total: R$ {toDecimal(fornecedor.valor)}
                        </h6>
                      )}
                    </div>
                    <ALink
                      href="#"
                      onClick={(e) => handleDeleteCart(e, fornecedor.id)}
                      className="btn btn-dark btn-ellipse btn-md"
                      style={{
                        borderRadius: 3,
                      }}
                    >
                      Deletar Carrinho
                      <i className="d-icon-times"></i>
                    </ALink>
                    <ALink
                      href={`/pages/checkout/${fornecedor.id}/provider`}
                      className="btn btn-primary btn-ellipse btn-md btn-rounded"
                    >
                      Finalizar Carrinho<i className="d-icon-arrow-right"></i>
                    </ALink>
                  </div>
                ))}
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default Cart;
