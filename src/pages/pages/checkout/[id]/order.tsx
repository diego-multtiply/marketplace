import { format } from "date-fns";
import Helmet from "react-helmet";

import ALink from "@/components/features/custom-link";

import { useEffect, useState } from "react";
import { toDecimal } from "@/utils";
import { useRouter } from "next/router";
import { getOrder } from "@/server/services/shop";
import PageLoading from "@/components/common/PageLoading";
import { useAppSelector } from "@/hooks/useStore";

const orderStatus = {
  P: "Pré Pedido",
  C: "Pedido Confirmado",
  R: "Recebido pelo Fornecedor",
  F: "Faturado",
  L: "Confirmado Recebimento",
  X: "Cancelado",
  E: "Conferência Final",
};

function Order() {
  const [pedido, setPedido] = useState(null);
  const [produtos, setProdutos] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const { user } = useAppSelector((state) => state.auth);

  const router = useRouter();

  useEffect(() => {
    async function loadOrder() {
      try {
        const { data } = await getOrder(Number(router.query.id));

        if (data.success) {
          setProdutos(data.pedido.itens);
          delete data.pedido.itens;
          setPedido(data.pedido);
        }
      } finally {
        setIsLoading(false);
      }
    }

    if (router.query.id) {
      loadOrder();
    }
  }, [router.query.id]);

  if (isLoading) return <PageLoading />;

  return (
    <main className="main order">
      <Helmet>
        <title>Multiweb | Pedido</title>
      </Helmet>

      <h1 className="d-none">Multiweb | Pedido</h1>

      <div className="page-content pt-7 pb-10 mb-10">
        <div className="container-fluid">
          <div className="order-message mr-auto ml-auto">
            <div className="icon-box d-inline-flex align-items-center">
              <div className="icon-box-icon mb-0">
                <svg
                  version="1.1"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  x="0px"
                  y="0px"
                  viewBox="0 0 50 50"
                  enableBackground="new 0 0 50 50"
                  xmlSpace="preserve"
                >
                  <g>
                    <path
                      fill="none"
                      strokeWidth="3"
                      strokeLinecap="round"
                      strokeLinejoin="bevel"
                      strokeMiterlimit="10"
                      d="
                                        M33.3,3.9c-2.7-1.1-5.6-1.8-8.7-1.8c-12.3,0-22.4,10-22.4,22.4c0,12.3,10,22.4,22.4,22.4c12.3,0,22.4-10,22.4-22.4
                                        c0-0.7,0-1.4-0.1-2.1"
                    ></path>
                    <polyline
                      fill="none"
                      strokeWidth="4"
                      strokeLinecap="round"
                      strokeLinejoin="bevel"
                      strokeMiterlimit="10"
                      points="
                                        48,6.9 24.4,29.8 17.2,22.3 	"
                    ></polyline>
                  </g>
                </svg>
              </div>
              <div className="icon-box-content text-left">
                <h5 className="icon-box-title font-weight-bold lh-1 mb-1">
                  Obrigado!
                </h5>
                <p className="lh-1 ls-m">O seu pedido foi recebido.</p>
              </div>
            </div>
          </div>

          <div className="order-results">
            <div className="overview-item">
              <span>Cód. Pedido:</span>
              <strong>{pedido?.id}</strong>
            </div>
            <div className="overview-item">
              <span>Status:</span>
              <strong>{orderStatus[pedido?.situacao]}</strong>
            </div>
            <div className="overview-item">
              <span>Data:</span>
              <strong>
                {format(new Date(pedido?.create_datetime), "dd/MM/yyyy HH:mm")}
              </strong>
            </div>
            {/* <div className="overview-item">
              <span>Email:</span>
              <strong>12345@gmail.com</strong>
            </div> */}
            <div className="overview-item">
              <span>Total:</span>
              <strong>R$ {toDecimal(pedido?.valor_total)}</strong>
            </div>
            {/* <div className="overview-item">
              <span>Payment method:</span>
              <strong>Cash on delivery</strong>
            </div> */}
          </div>

          <h2 className="title title-simple text-left pt-4 font-weight-bold text-uppercase">
            Detalhes do Pedido
          </h2>
          <div className="container-fluid mt-7">
            <div className="row">
              <div className="col-lg-8 mb-6 mb-lg-0 pr-lg-4">
                <div className="d-flex mt-4">
                  <div className="col-lg-12 col-md-12 pr-lg-4">
                    <table className="shop-table cart-table">
                      <thead>
                        <tr>
                          <th>
                            <span>Produto</span>
                          </th>
                          <th></th>
                          <th align="center">
                            <span>Valor Un.</span>
                          </th>
                          <th align="center">
                            <span>Qtde Un.</span>
                          </th>
                          <th align="center">
                            <span>Qtde</span>
                          </th>
                          <th align="center">
                            <span>Qtde Total</span>
                          </th>
                          <th align="center">Subtotal</th>
                        </tr>
                      </thead>
                      <tbody>
                        {produtos.map((item, index) => (
                          <tr key={"cart" + item.produto.nome}>
                            <td className="product-thumbnail">
                              <figure>
                                <ALink
                                  href={"/p/" + item.produto.slug}
                                  style={{
                                    width: 60,
                                  }}
                                >
                                  <img
                                    src={item.produto.imagem_principal}
                                    width="60"
                                    height="60"
                                    alt="product"
                                  />
                                </ALink>
                              </figure>
                            </td>
                            <td className="product-name">
                              <div className="product-name-section">
                                <ALink href={"/p/" + item.produto.slug}>
                                  {item.produto.nome}
                                </ALink>
                              </div>
                            </td>
                            <td className="product-subtotal p-0" align="center">
                              <span className="amount">
                                R$ {toDecimal(item.valor_unitario)}
                              </span>
                            </td>
                            <td className="product-quantity p-0" align="center">
                              <span className="amount">
                                {item.quantidade_unitaria}
                              </span>
                            </td>
                            <td className="product-quantity p-0" align="center">
                              <span className="amount">
                                {item.quantidade_embalagem}
                              </span>
                            </td>
                            <td className="product-quantity p-0" align="center">
                              <span className="amount">
                                {item.quantidade_unitaria *
                                  item.quantidade_embalagem}
                              </span>
                            </td>
                            <td className="product-price p-0" align="center">
                              <span className="amount">
                                R$ {toDecimal(item.valor_total)}
                              </span>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <aside className="col-lg-4 sticky-sidebar-wrapper checkout">
                <div
                  className="sticky-sidebar mt-1"
                  data-sticky-options="{'bottom': 50}"
                >
                  <div className="summary pt-5">
                    <h3 className="title title-simple text-left text-uppercase">
                      Seu Pedido
                    </h3>
                    {!user?.pessoa.cliente && (
                      <div className="row">
                        <div className="payment accordion col-12">
                          <h4 className="summary-subtitle ls-m pt-0 pb-1">
                            Cliente
                          </h4>
                          <label className="custom-control-label text-capitalize">
                            {pedido.cliente}
                          </label>
                        </div>
                      </div>
                    )}
                    <div className="row">
                      <div className="payment accordion col-12">
                        <h4 className="summary-subtitle ls-m pb-1">Endereço</h4>
                        {pedido.idendereco_entrega && (
                          <label className="custom-control-label text-capitalize">
                            {pedido.rua}, Nº {pedido.numero},{" "}
                            {pedido.complemento && `${pedido.complemento},`}{" "}
                            {pedido.bairro}, {pedido.cidade}, {pedido.estado} -{" "}
                            {pedido.cep}
                          </label>
                        )}
                      </div>
                    </div>
                    <div className="row">
                      <div className="payment accordion col-6">
                        <h4 className="summary-subtitle ls-m pb-1">
                          Prazo de Pagamento
                        </h4>
                        <label className="custom-control-label text-capitalize">
                          {pedido.prazo_pagamento}
                        </label>
                      </div>
                      <div className="payment accordion col-6">
                        <h4 className="summary-subtitle ls-m pb-1">
                          Forma de Pagamento
                        </h4>
                        <label className="custom-control-label text-capitalize">
                          {pedido.forma_pagamento}
                        </label>
                      </div>
                    </div>

                    <div className="payment accordion row">
                      <div className="col-6">
                        <h4 className="summary-subtitle ls-m pb-1">Frete</h4>
                        <label className="custom-control-label text-capitalize">
                          {pedido.frete === "CIF"
                            ? "CIF (Depósito Lojista)"
                            : pedido.frete}
                        </label>
                      </div>

                      {/* {form.frete === "CIF" && (
                            <div className="col-6">
                              <h4 className="summary-subtitle ls-m">
                                Desconto
                              </h4>
                              <input
                                type="text"
                                className="form-control mb-0"
                                id="descontoFrete"
                                name="descontoFrete"
                                disabled
                                value={
                                  fornecedor.fornecedor.tipo_desconto_frete ===
                                  "valor"
                                    ? `R$ ${toDecimal(form.descontoFrete)}`
                                    : `${toDecimal(form.descontoFrete)}%`
                                }
                                onChange={({ target: { value } }) =>
                                  setForm({
                                    ...form,
                                    descontoFrete: value,
                                  })
                                }
                              />
                            </div>
                          )} */}
                    </div>
                    <div className="row">
                      <div className="payment accordion col-12">
                        <h4 className="summary-subtitle ls-m pb-1">
                          Data de Entrega
                        </h4>
                        <label className="custom-control-label text-capitalize">
                          {pedido.data_entrega_descritivo}
                        </label>
                      </div>
                    </div>
                    {pedido.tipo_desconto && (
                      <div className="row">
                        <div className="payment accordion col-6">
                          <h4 className="summary-subtitle ls-m pb-1">
                            Tipo de Desconto
                          </h4>
                          <label className="custom-control-label text-capitalize">
                            {pedido.tipo_desconto === "V"
                              ? "Valor"
                              : "Percentual"}
                          </label>
                        </div>
                        <div className="payment accordion col-6">
                          <h4 className="summary-subtitle ls-m pb-1">Valor</h4>
                          <label className="custom-control-label text-capitalize">
                            {pedido.tipo_desconto === "V"
                              ? `R$ ${toDecimal(pedido.desconto)}`
                              : `${toDecimal(pedido.percentual_desconto)}%`}
                          </label>
                        </div>
                      </div>
                    )}

                    <div className="row">
                      <div className="payment accordion col-12">
                        <h4 className="summary-subtitle ls-m pb-1">
                          Observações
                        </h4>
                        <label className="custom-control-label text-capitalize">
                          {pedido.observacao || "Sem observações"}
                        </label>
                      </div>
                    </div>
                    <div className="payment accordion pb-0">
                      <div className="d-flex align-items-center justify-content-between">
                        <h4 className="summary-subtitle ls-m">
                          Quantidade Total
                        </h4>
                        <h4 className="summary-subtitle ls-m">
                          {pedido.quantidade}
                        </h4>
                      </div>
                      <div className="d-flex align-items-center justify-content-between">
                        <h4 className="summary-subtitle ls-m pt-0">
                          Sub Total
                        </h4>
                        <h4 className="summary-subtitle ls-m pt-0">
                          R$ {toDecimal(pedido.valor)}
                        </h4>
                      </div>
                      <div className="d-flex align-items-center justify-content-between">
                        <h4 className="summary-subtitle ls-m pt-0">Desconto</h4>
                        <h4 className="summary-subtitle ls-m pt-0">
                          R$ {toDecimal(pedido.desconto)}
                        </h4>
                      </div>
                      <div className="d-flex align-items-center justify-content-between">
                        <h4 className="summary-subtitle ls-m pt-0">
                          Preço Médio
                        </h4>
                        <h4 className="summary-subtitle ls-m pt-0">
                          R$ {toDecimal(pedido.preco_medio)}
                        </h4>
                      </div>
                    </div>
                    <div className="payment accordion d-flex align-items-center justify-content-between pb-0">
                      <h4 className="summary-subtitle ls-m">Total</h4>
                      <h4 className="summary-total-price ls-m pt-0">
                        R$ {toDecimal(pedido.valor_total)}
                      </h4>
                    </div>
                  </div>
                </div>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default Order;
