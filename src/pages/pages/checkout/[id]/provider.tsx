import { useState, useEffect, useMemo } from "react";
import { connect, useDispatch } from "react-redux";
import Helmet from "react-helmet";
import { toast } from "react-toastify";
import CurrencyInput from "react-currency-input-field";

import ALink from "@/components/features/custom-link";

import { formatObject, toDecimal } from "@/utils";
import Quantity from "@/components/features/quantity";
import { useRouter } from "next/router";
import {
  finishCart,
  getCart,
  getProvidersCart,
  removeFromCart,
  updateCart,
} from "@/server/services/shop";
import PageLoading from "@/components/common/PageLoading";
import Alert from "@/components/common/alert";
import { setProviders, setTotalProducts } from "@/store/cart";
import { useAppSelector } from "@/hooks/useStore";

import AlertPopup from "@/components/features/product/common/alert-popup";

function Checkout() {
  const [produtosOriginais, setProdutosOriginais] = useState([]);
  const [produtos, setProdutos] = useState([]);
  const [prazosPagamento, setPrazosPagamento] = useState([]);
  const [formasPagamento, setFormasPagamento] = useState([]);
  const [enderecos, setEnderecos] = useState([]);
  const [datasEntrega, setDatasEntrega] = useState([]);
  const [franquias, setFranquias] = useState([]);
  const [clientes, setClientes] = useState([]);
  const [fornecedor, setFornecedor] = useState({});
  const [loadingCart, setLoadingCart] = useState(false);
  const [error, setError] = useState({
    isVisible: false,
    message: "",
  });
  const [isLoading, setIsLoading] = useState(true);
  const [form, setForm] = useState({
    prazoPagamento: "",
    formaPagamento: "",
    endereco: "",
    frete: "",
    descontoFrete: "",
    desconto: 0,
    dataEntrega: {
      quinzena: "",
      ano: "",
    },
    observacao: "",
    tipoDesconto: "",
    percentualDesconto: 0,
  });
  const [selectedClient, setSelectedClient] = useState(null);
  const [selectedFranchise, setSelectedFranchise] = useState(null);

  const { fornecedores } = useAppSelector((state) => state.cart);
  const { isLogged, user } = useAppSelector((state) => state.auth);

  const dispatch = useDispatch();

  const router = useRouter();

  const totalQuantity = useMemo(() => {
    return produtos.reduce(
      (accumulator, currentValue) =>
        accumulator +
        currentValue.quantidade_unitaria * currentValue.quantidade,
      0
    );
  }, [produtos]);

  const subTotal = useMemo(() => {
    return produtos.reduce(
      (accumulator, currentValue) =>
        accumulator +
        currentValue.valor *
          currentValue.quantidade_unitaria *
          currentValue.quantidade,
      0
    );
  }, [produtos]);

  const discount = useMemo(() => {
    const discountValue =
      form.tipoDesconto === "V"
        ? form.desconto
        : form.tipoDesconto === "P"
        ? subTotal * (form.percentualDesconto / 100)
        : 0;

    const tipoDescontoFrete = fornecedor?.fornecedor?.tipo_desconto_frete;
    const descontoFrete = fornecedor?.fornecedor?.desconto_frete;

    let discountFreight =
      tipoDescontoFrete === "valor"
        ? descontoFrete
        : tipoDescontoFrete === "percentual"
        ? subTotal * (descontoFrete / 100)
        : 0;

    return discountFreight + discountValue;
  }, [
    subTotal,
    fornecedor,
    form.desconto,
    form.tipoDesconto,
    form.percentualDesconto,
  ]);

  const totalValue = useMemo(() => {
    return subTotal - discount;
  }, [subTotal, discount]);

  const priceMedium = useMemo(() => {
    return totalValue / totalQuantity;
  }, [totalValue, totalQuantity]);

  async function loadCart(uf?: string) {
    try {
      const { data } = await getCart(
        Number(router.query.id),
        uf,
        selectedFranchise,
        selectedClient
      );

      if (!uf) {
        setProdutosOriginais(data.data.produtos);
        setProdutos(data.data.produtos);
        setPrazosPagamento(data.data.prazosPagamento);
        setFormasPagamento(data.data.formasPagamento);
        setEnderecos(data.data.enderecos);
        setFornecedor(data.data.fornecedor);
        setDatasEntrega(data.data.datasEntrega);
        setFranquias(data.data.franquias);
        setClientes(data.data.clientes);

        setForm({
          ...form,
          frete: data.data.fornecedor.fornecedor.frete,
          descontoFrete: data.data.fornecedor.fornecedor.desconto_frete,
          endereco: form.endereco
            ? form.endereco
            : data.data.enderecos.find((item) => item.principal === "S")?.id,
          dataEntrega: {
            ano: data.data.datasEntrega[0].ano,
            quinzena: data.data.datasEntrega[0].id,
          },
        });
      } else {
        setProdutosOriginais(data.data.produtos);
        setProdutos(data.data.produtos);
      }
    } finally {
      setIsLoading(false);

      document
        .getElementById("products-table")
        ?.classList.remove("load-more-overlay", "loading");
    }
  }

  useEffect(() => {
    if (router.query.id) {
      loadCart();
    }
  }, [router.query.id]);

  useEffect(() => {
    if (selectedFranchise || selectedClient) {
      loadCart();
    }
  }, [selectedFranchise, selectedClient]);

  const compareItems = () => {
    if (produtosOriginais.length !== produtos.length) {
      return false;
    }

    for (let i = 0; i < produtos.length; i++) {
      if (produtosOriginais[i].quantidade !== produtos[i].quantidade) {
        return false;
      }
    }

    return true;
  };

  async function updateCartProducts(e) {
    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setLoadingCart(true);

    try {
      const productsCart = produtos.map((item) => ({
        id: item.id,
        quantidade: item.quantidade,
      }));

      const { data } = await updateCart(productsCart, Number(router.query.id));

      if (data.success) {
        await loadCart(
          enderecos.find((item) => item.id === form.endereco)?.estado.sigla
        );
      }
    } finally {
      setLoadingCart(false);
      currentTarget.classList.remove("load-more-overlay", "loading");
    }
  }

  async function onChangeQuantity(index: number, quantity: number) {
    const newProductsArr: any[] = [...produtos];

    newProductsArr[index] = {
      ...newProductsArr[index],
      quantidade: quantity,
    };

    setProdutos(newProductsArr);
  }

  async function handleSubmit(e, type: string) {
    setError({
      isVisible: false,
      message: "",
    });

    if (!compareItems()) {
      return toast(
        <AlertPopup message="Atualize o carrinho antes de prosseguir." />,
        {
          position: "top-right",
        }
      );
    }

    if (
      !form.prazoPagamento ||
      !form.formaPagamento ||
      !form.endereco ||
      !form.dataEntrega
    ) {
      return toast(
        <AlertPopup message="Há dados faltando. Preencha todos os campos marcados com *." />,
        {
          position: "top-right",
        }
      );
    }

    setLoadingCart(true);

    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    try {
      const cart = {
        itens: produtos
          .filter((item) => item.idtabela_preco)
          .map((item) => ({
            idproduto: item.id,
            quantidade: item.quantidade,
            quantidade_unitaria: item.quantidade_unitaria,
            valor: item.valor,
            idtabela_preco: item.idtabela_preco,
          })),
        prazoPagamento: Number(form.prazoPagamento),
        formaPagamento: Number(form.formaPagamento),
        endereco: form.endereco,
        frete: form.frete,
        descontoFrete: form.descontoFrete,
        quinzenaEntrega: Number(form.dataEntrega.quinzena),
        anoEntrega: Number(form.dataEntrega.ano),
        observacoes: form.observacao,
        quantidade: totalQuantity,
        desconto: form.desconto,
        subTotal: subTotal,
        valorTotal: totalValue,
        situacao: type,
        tipoDesconto: form.tipoDesconto,
        percentualDesconto: form.percentualDesconto,
      };

      if (!user?.pessoa.cliente) {
        cart.idcliente = selectedClient;
      }

      const { data } = await finishCart(cart, Number(router.query.id));

      if (data.success) {
        if (isLogged) {
          const { data: providersCartData } = await getProvidersCart();

          if (user?.pessoa.fornecedor) {
            dispatch(
              setTotalProducts(
                formatObject(providersCartData.data[0])?.quantidade_produtos
              )
            );
          } else {
            dispatch(setProviders(providersCartData.data));
          }
        }

        router.push(`/pages/checkout/${data.id}/order`);
      } else if (data.message) {
        toast(<AlertPopup message={data.message} />, {
          position: "top-right",
        });
      }
    } finally {
      setLoadingCart(false);
      currentTarget.classList.remove("load-more-overlay", "loading");
    }
  }

  async function handleRemoveFromCart(e, idproduto: number) {
    e.preventDefault();

    let currentTarget = e.currentTarget;
    currentTarget.classList.add("load-more-overlay", "loading");

    setLoadingCart(true);

    try {
      const { data } = await removeFromCart(idproduto, Number(router.query.id));

      if (data.success) {
        if (produtos.length === 1) {
          if (isLogged) {
            const { data: providersCartData } = await getProvidersCart();

            if (user?.pessoa.fornecedor) {
              dispatch(
                setTotalProducts(
                  formatObject(providersCartData.data[0])?.quantidade_produtos
                )
              );
            } else {
              dispatch(setProviders(providersCartData.data));
            }
          }
        }
        await loadCart(
          enderecos.find((item) => item.id === form.endereco)?.estado.sigla
        );
      }
    } finally {
      setLoadingCart(false);
      currentTarget.classList.remove("load-more-overlay", "loading");
    }
  }

  if (isLoading) return <PageLoading />;

  return (
    <main className="main checkout border-no">
      <Helmet>
        <title>Multiweb | Checkout</title>
      </Helmet>

      <h1 className="d-none">Multiweb - Checkout</h1>

      <div
        className={`page-content pt-7 pb-10 ${
          produtos.length > 0 ? "mb-10" : "mb-2"
        }`}
      >
        <div className="container-fluid mt-7">
          {produtos.length > 0 ? (
            <>
              <div className="row">
                <div className="col-lg-8 mb-6 mb-lg-0 pr-lg-4">
                  <div className="d-flex mt-4">
                    <div className="col-lg-12 col-md-12 pr-lg-4">
                      <table
                        className="shop-table cart-table"
                        id="products-table"
                      >
                        <thead>
                          <tr>
                            <th>
                              <span>Produto</span>
                            </th>
                            <th></th>
                            <th align="center">
                              <span>Valor Un.</span>
                            </th>
                            <th align="center">
                              <span>Qtde Un.</span>
                            </th>
                            <th align="center">
                              <span>Qtde</span>
                            </th>
                            <th align="center">
                              <span>Qtde Total</span>
                            </th>
                            <th align="center">Subtotal</th>
                            <th align="center"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {produtos.map((item, index) => (
                            <tr
                              key={"cart" + item.produto}
                              style={{
                                background:
                                  user?.pessoa.cliente && !item.idtabela_preco
                                    ? "rgba(255, 99, 71, 0.2)"
                                    : "white",
                                position: "relative",
                              }}
                            >
                              <td className="product-thumbnail">
                                <figure className="ml-2">
                                  <ALink
                                    href={"/p/" + item.slug}
                                    style={{
                                      width: 60,
                                    }}
                                  >
                                    <img
                                      src={item.imagem}
                                      width="60"
                                      height="60"
                                      alt="product"
                                    />
                                  </ALink>
                                </figure>
                              </td>
                              <td className="product-name" style={{}}>
                                <div
                                  className="product-name-section"
                                  style={{
                                    textDecoration:
                                      user?.pessoa.cliente &&
                                      !item.idtabela_preco
                                        ? "line-through"
                                        : "none",
                                  }}
                                >
                                  <ALink href={"/p/" + item.slug}>
                                    {item.produto}
                                  </ALink>
                                </div>
                                {user?.pessoa.cliente &&
                                  !item.idtabela_preco && (
                                    <div
                                      className="product-name-section"
                                      style={{
                                        width: "auto",
                                        display: "flex",
                                        justifyContent: "flex-start",
                                      }}
                                    >
                                      <div
                                        style={{
                                          background: "#FAFAFA",
                                          borderRadius: "8px",
                                          padding: "4px 8px",
                                        }}
                                      >
                                        Produto indisponível para este endereço
                                      </div>
                                    </div>
                                  )}
                              </td>
                              <td
                                className="product-subtotal p-0"
                                align="center"
                              >
                                {item.valor === "-" || !item.idtabela_preco ? (
                                  <span className="amount">-</span>
                                ) : (
                                  <span className="amount">
                                    R$ {toDecimal(item.valor)}
                                  </span>
                                )}
                              </td>
                              <td
                                className="product-quantity p-0"
                                align="center"
                              >
                                <span className="amount">
                                  {item.quantidade_unitaria}
                                </span>
                              </td>
                              <td
                                className="product-quantity p-0"
                                align="center"
                              >
                                {user?.pessoa.cliente &&
                                !item.idtabela_preco ? (
                                  <span className="amount">
                                    {item.quantidade}
                                  </span>
                                ) : (
                                  <Quantity
                                    qty={item.quantidade}
                                    max={10000}
                                    onChangeQty={(qty) => {
                                      onChangeQuantity(index, qty);
                                    }}
                                  />
                                )}
                              </td>
                              <td
                                className="product-quantity p-0"
                                align="center"
                              >
                                <span className="amount">
                                  {item.quantidade_unitaria * item.quantidade}
                                </span>
                              </td>
                              <td className="product-price p-0" align="center">
                                {item.valor === "-" || !item.idtabela_preco ? (
                                  <span className="amount">-</span>
                                ) : (
                                  <span className="amount">
                                    R${" "}
                                    {toDecimal(
                                      item.valor *
                                        item.quantidade_unitaria *
                                        item.quantidade
                                    )}
                                  </span>
                                )}
                              </td>
                              <td className="product-close">
                                <ALink
                                  href="#"
                                  className="product-remove ml-3"
                                  title="Remover produto"
                                  onClick={(e) => {
                                    handleRemoveFromCart(e, item.id);
                                  }}
                                >
                                  <i className="fas fa-times"></i>
                                </ALink>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                      <div className="cart-actions mb-6 pt-4">
                        <ALink
                          href="/shop"
                          className="btn btn-primary btn-md btn-rounded btn-icon-left mr-4 mb-4"
                        >
                          <i className="d-icon-arrow-left"></i>Continue
                          Comprando
                        </ALink>
                        <div className="d-flex">
                          <button
                            className={`btn btn-outline btn-primary btn-md ${
                              compareItems() ? " btn-disabled" : ""
                            }`}
                            style={{
                              borderRadius: 3,
                            }}
                            onClick={updateCartProducts}
                            disabled={compareItems() || loadingCart}
                          >
                            Atualizar Carrinho
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <aside className="col-lg-4 sticky-sidebar-wrapper">
                  <div
                    className="sticky-sidebar mt-1"
                    data-sticky-options="{'bottom': 50}"
                  >
                    <div className="summary pt-5">
                      <h3 className="title title-simple text-left text-uppercase">
                        Seu Pedido
                      </h3>
                      {!user?.pessoa.cliente && (
                        <div className="row">
                          <div className="col-12">
                            <div className="payment accordion ">
                              <h4 className="summary-subtitle ls-m pt-0">
                                Franquia*
                              </h4>
                              <div className="select-box">
                                <select
                                  name="franquias"
                                  className="form-control mb-0"
                                  value={selectedFranchise}
                                  onChange={({ target: { value } }) => {
                                    setSelectedFranchise(value);
                                    setSelectedClient(null);
                                  }}
                                >
                                  <option value=""></option>
                                  {franquias.map((item) => (
                                    <option value={item.id} key={item.id}>
                                      {item.descricao}
                                    </option>
                                  ))}
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                      {!!clientes.length && (
                        <div className="row">
                          <div className="col-12">
                            <div className="payment accordion ">
                              <h4 className="summary-subtitle ls-m">
                                Cliente*
                              </h4>
                              <div className="select-box">
                                <select
                                  name="clientes"
                                  className="form-control mb-0"
                                  value={selectedClient}
                                  onChange={({ target: { value } }) => {
                                    setSelectedClient(value);
                                    setForm({
                                      ...form,
                                      endereco: "",
                                    });
                                  }}
                                >
                                  <option value=""></option>
                                  {clientes.map((item) => (
                                    <option value={item.id} key={item.id}>
                                      {item.fantasia} - {item.documento}
                                    </option>
                                  ))}
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                      {!!enderecos.length && (
                        <div className="payment accordion row">
                          <div className="col-12">
                            <h4
                              className={`summary-subtitle ls-m ${
                                user?.pessoa.cliente && "pt-0"
                              }`}
                            >
                              Endereço
                            </h4>
                            <div>
                              {enderecos.map((item) => (
                                <div className="custom-radio" key={item.id}>
                                  <input
                                    type="radio"
                                    id={item.id}
                                    name="endereco"
                                    className="custom-control-input"
                                    checked={form.endereco === item.id}
                                    onChange={() => {
                                      setForm({
                                        ...form,
                                        endereco: item.id,
                                      });

                                      document
                                        .getElementById("products-table")
                                        ?.classList.add(
                                          "load-more-overlay",
                                          "loading"
                                        );

                                      loadCart(item.estado.sigla);
                                    }}
                                  />
                                  <label
                                    className="custom-control-label text-capitalize"
                                    htmlFor={item.id}
                                  >
                                    {item.rua}, Nº {item.numero},{" "}
                                    {item.complemento && `${item.complemento},`}{" "}
                                    {item.bairro}, {item.cidade.nome},{" "}
                                    {item.estado.sigla} - {item.cep}
                                  </label>
                                </div>
                              ))}
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="row">
                        <div className="payment accordion col-6">
                          <h4 className="summary-subtitle ls-m">
                            Prazo de Pagamento*
                          </h4>
                          <div className="select-box">
                            <select
                              name="prazosPagamento"
                              className="form-control mb-0"
                              value={form.prazoPagamento}
                              onChange={({ target: { value } }) =>
                                setForm({
                                  ...form,
                                  prazoPagamento: value,
                                })
                              }
                            >
                              <option value=""></option>
                              {prazosPagamento.map((item) => (
                                <option value={item.id} key={item.id}>
                                  {item.descricao}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="payment accordion col-6">
                          <h4 className="summary-subtitle ls-m">
                            Forma de Pagamento*
                          </h4>
                          <div className="select-box">
                            <select
                              name="formasPagamento"
                              className="form-control mb-0"
                              value={form.formaPagamento}
                              onChange={({ target: { value } }) =>
                                setForm({
                                  ...form,
                                  formaPagamento: value,
                                })
                              }
                            >
                              <option value=""></option>
                              {formasPagamento.map((item) => (
                                <option value={item.id} key={item.id}>
                                  {item.descricao}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="payment accordion row">
                        <div className="col-6">
                          <h4 className="summary-subtitle ls-m">Frete</h4>
                          <div className="select-box">
                            <select
                              name="frete"
                              className="form-control mb-0"
                              disabled
                              value={form.frete}
                            >
                              <option value="CIF">
                                CIF (Depósito Lojista)
                              </option>
                              <option value="FOB">FOB</option>
                            </select>
                          </div>
                        </div>
                        {form.frete === "CIF" && (
                          <div className="col-6">
                            <h4 className="summary-subtitle ls-m">Desconto</h4>
                            <input
                              type="text"
                              className="form-control mb-0"
                              id="descontoFrete"
                              name="descontoFrete"
                              disabled
                              value={
                                fornecedor.fornecedor.tipo_desconto_frete ===
                                "valor"
                                  ? `R$ ${toDecimal(form.descontoFrete)}`
                                  : `${toDecimal(form.descontoFrete)}%`
                              }
                              onChange={({ target: { value } }) =>
                                setForm({
                                  ...form,
                                  descontoFrete: value,
                                })
                              }
                            />
                          </div>
                        )}
                      </div>
                      <div className="payment accordion row">
                        <div className="col-12">
                          <h4 className="summary-subtitle ls-m">
                            Data de Entrega*
                          </h4>
                          <div className="select-box">
                            <select
                              name="dataEntrega"
                              className="form-control mb-0"
                              value={
                                form.dataEntrega.quinzena +
                                ":" +
                                form.dataEntrega.ano
                              }
                              onChange={({ target: { value } }) =>
                                setForm({
                                  ...form,
                                  dataEntrega: {
                                    quinzena: value.split(":")[0],
                                    ano: value.split(":")[1],
                                  },
                                })
                              }
                            >
                              {datasEntrega.map((item) => (
                                <option value={item.id + ":" + item.ano}>
                                  {item.descricao}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>
                      {!user?.pessoa.cliente && (
                        <div className="payment accordion row">
                          <div className="col-6">
                            <h4 className="summary-subtitle ls-m">
                              Tipo de Desconto
                            </h4>
                            <div className="select-box">
                              <select
                                name="tipoDesconto"
                                className="form-control mb-0"
                                value={form.tipoDesconto}
                                onChange={({ target: { value } }) =>
                                  setForm({
                                    ...form,
                                    tipoDesconto: value,
                                    desconto: 0,
                                    percentualDesconto: 0,
                                  })
                                }
                              >
                                <option value="">Sem Desconto</option>
                                <option value="V">Valor</option>
                                <option value="P">Percentual</option>
                              </select>
                            </div>
                          </div>
                          {form.tipoDesconto === "V" ? (
                            <div className="col-6">
                              <h4 className="summary-subtitle ls-m">Valor</h4>

                              <CurrencyInput
                                className="form-control mb-0"
                                placeholder="R$ 0,00"
                                decimalScale={2}
                                intlConfig={{
                                  locale: "pt-BR",
                                  currency: "BRL",
                                }}
                                onValueChange={(value, name, values) =>
                                  setForm({
                                    ...form,
                                    desconto: values?.float,
                                  })
                                }
                              />
                            </div>
                          ) : form.tipoDesconto === "P" ? (
                            <div className="col-6">
                              <h4 className="summary-subtitle ls-m">Valor</h4>

                              <CurrencyInput
                                className="form-control mb-0"
                                placeholder="0,00%"
                                decimalScale={2}
                                suffix="%"
                                onValueChange={(value, name, values) =>
                                  setForm({
                                    ...form,
                                    percentualDesconto: values?.float,
                                  })
                                }
                              />
                            </div>
                          ) : null}
                        </div>
                      )}
                      <div className="payment accordion">
                        <h4 className="summary-subtitle ls-m">Observações</h4>
                        <textarea
                          className="form-control pb-2 pt-2 mb-0"
                          cols="30"
                          rows="5"
                          placeholder="Observações sobre seu pedido"
                        ></textarea>
                      </div>
                      <div className="payment accordion pb-0">
                        <div className="d-flex align-items-center justify-content-between">
                          <h4 className="summary-subtitle ls-m">
                            Quantidade Total
                          </h4>
                          <h4 className="summary-subtitle ls-m">
                            {totalQuantity}
                          </h4>
                        </div>
                        <div className="d-flex align-items-center justify-content-between">
                          <h4 className="summary-subtitle ls-m pt-0">
                            Sub Total
                          </h4>
                          <h4 className="summary-subtitle ls-m pt-0">
                            R$ {subTotal ? toDecimal(subTotal) : "-"}
                          </h4>
                        </div>
                        <div className="d-flex align-items-center justify-content-between">
                          <h4 className="summary-subtitle ls-m pt-0">
                            Desconto
                          </h4>
                          <h4 className="summary-subtitle ls-m pt-0">
                            R$ {toDecimal(discount)}
                          </h4>
                        </div>
                        <div className="d-flex align-items-center justify-content-between">
                          <h4 className="summary-subtitle ls-m pt-0">
                            Preço Médio
                          </h4>
                          <h4 className="summary-subtitle ls-m pt-0">
                            R$ {priceMedium ? toDecimal(priceMedium) : "-"}
                          </h4>
                        </div>
                      </div>
                      <div className="payment accordion d-flex align-items-center justify-content-between pb-0">
                        <h4 className="summary-subtitle ls-m">Total</h4>
                        <h4 className="summary-total-price ls-m pt-0">
                          R$ {totalValue ? toDecimal(totalValue) : "-"}
                        </h4>
                      </div>
                      {error.isVisible && (
                        <Alert
                          type="warning"
                          text={error.message}
                          className="mb-3 mt-3"
                        />
                      )}
                      {user?.pessoa.cliente ? (
                        <div className="row mt-5">
                          <div className="col-6">
                            <button
                              type="submit"
                              className="btn btn-outline btn-secondary"
                              disabled={loadingCart}
                              onClick={(e) => handleSubmit(e, "P")}
                              style={{
                                borderRadius: 3,
                              }}
                            >
                              Finalizar Pré Pedido
                            </button>
                          </div>

                          <div className="col-6">
                            <button
                              type="submit"
                              className="btn btn-dark btn-order"
                              disabled={loadingCart}
                              onClick={(e) => handleSubmit(e, "C")}
                              style={{
                                borderRadius: 3,
                              }}
                            >
                              Finalizar pedido
                            </button>
                          </div>
                        </div>
                      ) : (
                        <div className="row mt-5">
                          <div className="col-12">
                            <button
                              type="submit"
                              className="btn btn-dark btn-order"
                              disabled={loadingCart}
                              onClick={(e) => handleSubmit(e, "P")}
                              style={{
                                borderRadius: 3,
                              }}
                            >
                              Finalizar pedido
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </aside>
              </div>
            </>
          ) : (
            <div className="empty-cart text-center">
              <p>
                {fornecedores.length === 0
                  ? "Seu carrinho está vazio."
                  : "Selecione um carrinho para finalizar."}
              </p>
              <i className="cart-empty d-icon-bag"></i>
              <p className="return-to-shop mb-0">
                <ALink
                  className="button wc-backward btn btn-dark btn-md"
                  href={fornecedores.length === 0 ? "/shop" : "/pages/cart"}
                >
                  {fornecedores.length === 0
                    ? "Voltar para a Loja"
                    : "Voltar para o Carrinho"}
                </ALink>
              </p>
            </div>
          )}
        </div>
      </div>
    </main>
  );
}

function mapStateToProps(state) {
  return {
    cartList: state.cart.data ? state.cart.data : [],
  };
}

export default connect(mapStateToProps)(Checkout);
