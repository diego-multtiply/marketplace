/* eslint-disable @next/next/no-sync-scripts */
/* eslint-disable @next/next/no-css-tags */
/* eslint-disable @next/next/google-font-display */
import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        {/* <base href="/react/riode/demo-market2/"></base> */}
        {/* <title>Riode - React eCommerce Template</title> */}
        <link rel="icon" href="/images/icons/favicon.png" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CJost:400,600,700"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="/vendor/riode-fonts/riode-fonts.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="/vendor/fontawesome-free/css/all.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="/vendor/owl-carousel/owl.carousel.min.css"
        />
      </Head>
      <body>
        <Main />

        <script src="/js/jquery.min.js"></script>

        <NextScript />
      </body>
    </Html>
  );
}
