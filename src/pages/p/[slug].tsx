import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Helmet from "react-helmet";
import imagesLoaded from "imagesloaded";

import OwlCarousel from "@/components/features/owl-carousel";

import MediaOne from "@/components/partials/product/media/media-one";
import DetailOne from "@/components/partials/product/detail/detail-one";
import DescOne from "@/components/partials/product/desc/desc-one";
import RelatedProducts from "@/components/partials/product/related-products";

import { mainSlider17 } from "@/utils/data/carousel";
import { getProduct } from "@/server/services/products";
import { IProduct } from "@/common/types/products";
import useShop from "@/hooks/useShop";
import { useAppSelector } from "@/hooks/useStore";

function ProductDefault() {
  const [loaded, setLoadingState] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [product, setProduct] = useState(null);

  const related = [];

  const slug: string = useRouter().query.slug;

  useEffect(() => {
    async function loadProduct() {
      setIsLoading(true);

      try {
        const response = await getProduct(slug);

        const data: IProduct = response.data.data;

        setProduct({
          ...data,
          id: data.id,
          slug: data.slug,
          name: data.nome,
          price: data.preco,
          minPrice: Number(data.preco_minimo),
          maxPrice: Number(data.preco_maximo),
          discount: 0,
          short_description: data.descricao,
          complementar_description: data.descricao_complementar,
          collection: data.descricao_colecao,
          sku: data.sku,
          stock: 80,
          ratings: 5,
          reviews: 1,
          sale_count: 30,
          is_featured: false,
          is_new: false,
          is_sale: false,
          is_review: false,
          is_top: false,
          until: null,
          dimensions: [
            {
              name: "Altura",
              value: data.altura,
            },
            {
              name: "Largura",
              value: data.largura,
            },
            {
              name: "Comprimento",
              value: data?.comprimento,
            },
          ],
          pictures: data.imagens.length
            ? data.imagens.map((image) => ({
                url:
                  "https://cyhnwguzha.cloudimg.io/" +
                  image.url +
                  "?width=300&height=338",
                width: 300,
                height: 338,
              }))
            : [
                {
                  url: "/uploads/images/demo-1/products/product-15-1-300x338.jpg",
                  width: 300,
                  height: 338,
                },
              ],
          small_pictures: null,
          large_pictures: data.imagens.length
            ? data.imagens.map((image) => ({
                url:
                  "https://cyhnwguzha.cloudimg.io/" +
                  image.url +
                  "?width=800&height=900",
                width: 800,
                height: 900,
              }))
            : [
                {
                  url: "/uploads/images/demo-1/products/product-15-1-300x338.jpg",
                  width: 800,
                  height: 900,
                },
              ],
          categories: [
            {
              name: data.descricao_departamento,
              slug: data.slug_departamento,
            },
            {
              name: data.descricao_grupo_principal,
              slug: data.slug_grupo_principal,
            },
            {
              name: data.descricao_grupo_secundario,
              slug: data.slug_grupo_secundario,
            },
          ],
          tags: [],
          brands: [
            {
              name: data.marca,
            },
          ],
          variants: data.variacoes.map((item) => ({
            price: 0,
            sale_price: null,
            color: item.cor
              ? {
                  name: item.cor,
                  slug: item.cor,
                  color: "#fff",
                }
              : null,
            size: item.tamanho
              ? {
                  name: item.tamanho,
                  slug: item.tamanho,
                  size: item.tamanho,
                }
              : item.numeracao
              ? {
                  name: item.numeracao,
                  slug: item.numeracao,
                  size: item.numeracao,
                }
              : null,
          })),
          quantity: data.volumes,
          un: data.unidade,
        });

        setIsLoading(false);
      } catch (err) {
      } finally {
        setIsLoading(false);
      }
    }

    if (slug) {
      loadProduct();
    }
  }, [slug]);

  useEffect(() => {
    if (!isLoading && product)
      imagesLoaded("main")
        .on("done", function () {
          setLoadingState(true);
        })
        .on("progress", function () {
          setLoadingState(false);
        });
    if (isLoading) setLoadingState(false);
  }, [isLoading, product]);

  const stickyContentHandler = () => {
    let stickyContent = document.querySelector(".product-sticky-content");
    let height = 0;
    let offsetHeight = 0;

    if (stickyContent) {
      height = stickyContent.offsetHeight;

      if (window.scrollY > 600 && window.innerWidth > 991) {
        stickyContent.classList.add("fixed");
        if (document.querySelector(".sticky-header.sticky-content"))
          offsetHeight = document?.querySelector(
            ".sticky-header.sticky-content"
          )?.offsetHeight;
        else offsetHeight = 88;

        if (!document.querySelector(".sticky-product-wrapper")) {
          let stickyWrapper = document.createElement("div");
          stickyWrapper.className = "sticky-product-wrapper";
          stickyContent.parentNode.insertBefore(stickyWrapper, stickyContent);
          document
            ?.querySelector(".sticky-product-wrapper")
            .insertAdjacentElement("beforeend", stickyContent);
          document
            ?.querySelector(".sticky-product-wrapper")
            .setAttribute("style", "height: " + height + "px");
        }

        if (
          !document
            .querySelector(".sticky-product-wrapper")
            .getAttribute("style")
        ) {
          document
            .querySelector(".sticky-product-wrapper")
            .setAttribute("style", "height: " + height + "px");
        }
        document
          .querySelector(".product-sticky-content")
          .setAttribute("style", `top: ${offsetHeight}px`);
      } else {
        if (document.querySelector(".sticky-product-wrapper")) {
          document
            .querySelector(".sticky-product-wrapper")
            .setAttribute("style", "");
        }
        document
          .querySelector(".product-sticky-content")
          .classList.remove("fixed");
      }
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", stickyContentHandler, true);

    return () => {
      window.removeEventListener("scroll", stickyContentHandler, true);
    };
  }, []);

  if (!slug) return "";

  return (
    <main className="main mt-6 single-product">
      <Helmet>
        <title>Multiweb | {`${product?.name}`}</title>
      </Helmet>

      <h1 className="d-none">Multiweb | {product?.name}</h1>

      {product && (
        <div className={`page-content mb-10 pb-6 ${loaded ? "" : "d-none"}`}>
          <div className="container skeleton-body vertical">
            <div className="product product-single row mb-7">
              <div className="col-md-6 sticky-sidebar-wrapper">
                <MediaOne product={product} />
              </div>

              <div className="col-md-6">
                <DetailOne
                  data={product}
                  isStickyCart={true}
                  adClass="mt-4 mt-md-0"
                />
              </div>
            </div>

            <DescOne product={product} isGuide={false} />

            <RelatedProducts products={related} />
          </div>
        </div>
      )}
      {!loaded && isLoading && (
        <div className="skeleton-body container mb-10">
          <div className="row mb-7">
            <div className="col-md-6 pg-vertical sticky-sidebar-wrapper">
              <div className="skel-pro-gallery"></div>
            </div>

            <div className="col-md-6">
              <div className="skel-pro-summary mt-4 mt-md-0"></div>
            </div>
          </div>

          <div className="skel-pro-tabs"></div>

          <section className="pt-3 mt-4">
            <h2 className="title justify-content-center">
              Produtos Relacionados
            </h2>

            <OwlCarousel
              adClass="owl-carousel owl-theme owl-nav-full"
              options={mainSlider17}
            >
              {[1, 2, 3, 4, 5, 6].map((item) => (
                <div
                  className="product-loading-overlay"
                  key={"popup-skel-" + item}
                ></div>
              ))}
            </OwlCarousel>
          </section>
        </div>
      )}
    </main>
  );
}

export default ProductDefault;
